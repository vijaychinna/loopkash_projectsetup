import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styles: []
})
export class ContactUsComponent extends BaseComponent implements OnInit {
  public contact: any = {};
  public submittedContact: boolean = false;
  constructor(inj: Injector) {
    super(inj);
  }
  ngOnInit() {
  }

  /*************************************************************/
  // // Submit contact
  /*************************************************************/
  submitContact(form) {
    this.submittedContact = true;
    if (form.valid) {
      this.spinner.show();
      const contactData = {
        contactname: this.contact.name,
        emailaddress: this.contact.email.toLowerCase(),
        message: this.contact.comment,
        userId: this.getToken("_id")
      };
      this.commonService
        .callApi("contactUs", contactData, "post")
        .then(success => {
          this.spinner.hide();
          if (success.settings.success === 1) {
            this.router.navigate(['/user/dashboard'])
            this.popToast("success", success.settings.message);
          } else {
            this.popToast("error", success.settings.message);
          }
        }, error => {
        });
    }
  }
  /*************************************************************/
}
