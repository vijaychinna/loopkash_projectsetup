import {Component, OnInit, Injector, TemplateRef} from '@angular/core';
import {BaseComponent} from './../../common/commonComponent';
import {IBreadcrumbs} from './../../common/interfaces';

@Component(
    {selector: 'app-profile', templateUrl: './profile.component.html', styles: []}
)
export class ProfileComponent extends BaseComponent implements OnInit {

    breadcrumbs: IBreadcrumbs[];
    constructor(inj : Injector) {
        super(inj);
        switch (this.getToken('profileRole')) {
            case "SP":
                this
                    .spinner
                    .hide();
                // this.getServiceProviderProfile()
                break;
            case "ESP":
                this
                    .spinner
                    .hide();
                // this.getEscrowProfile()
                break;
            case "USR":
                this
                    .spinner
                    .hide();
                this.getUserProfile()
                break;
            case "PTR":
                this
                    .spinner
                    .hide();
                this.getPartnerProfile()

                break;
            case "PM":
                this
                    .spinner
                    .hide();
                // this.getPMProfile()
                break;
        }
    }
    onUpdateProfile() {
        switch (this.getToken('profileRole')) {
            case "SP":
                this
                    .spinner
                    .hide();
                // this.getServiceProviderProfile()
                break;
            case "ESP":
                this
                    .spinner
                    .hide();
                // this.getEscrowProfile()
                break;
            case "USR":
                this
                    .spinner
                    .hide();
                this.onUpdateUserProfile()
                break;
            case "PTR":
                this
                    .spinner
                    .hide();
                this.onUpdatePartnerProfile()
                break;
            case "PM":
                this
                    .spinner
                    .hide();
                // this.getPMProfile()
                break;
        }
    }
    ngOnInit() {
        this.setBreadcrumbs();
    }

    public setBreadcrumbs() {
        this.breadcrumbs = [
            {
                localeKey: 'User',
                url: null
            },
            // Different according to user Role
            {
                localeKey: 'profile',
                url: null
            }
        ];
    }

    /*************************************************************/
    // Open Change Password Model
    /*************************************************************/
    openChangePassModal(template : TemplateRef<any>) {
        this.bsModalRef = this
            .modalService
            .show(template, {class: "modal-dialog-centered auth-modal"});
        this.userPassword = {};
        this.submittedPassword = false;
    }
    /*************************************************************/

    /*************************************************************/
    //  Submit change password
    /*************************************************************/
    public datapi = {}
    public data = {}
    public dataImage = {}

    displayCounterPI(valpi) {
        this.datapi = valpi
        console.log(valpi);
        //  console.log( datapi);
    }
    displayCounterAddress(vala) {
        this.data = vala
        console.log(vala); console.log(this.data);
    }
    displayphoto(valImage) {
        console.log(valImage);

        this.dataImage = valImage
    }

    submittedProfile: Boolean;
    onUpdateUserProfile() {
        console.log("bhumika is grate=============")
        console.log(this.data);
        console.log(this.datapi);
        if (this.data) {
            var adddata = Object.assign(this.data, this.user, this.dataImage)
            console.log("bhumika is grate 1", adddata)

        } else {
            var adddata = Object.assign(this.user, this.datapi, this.dataImage)
            console.log("bhumika is grate 2", adddata)

        }
        console.log("bhumika is grate", adddata)
        this.submittedProfile = true;
        this.commonService.callApi("updateUserProfile", adddata, 'put', '', true, true).then(success => {
                this.spinner.hide();
                if (success.settings.status === 1) {
                    this.popToast("success", success.settings.message);
                    // this.setToken("username", success.data.name) this.setToken("profileimage",
                    // success.data.profileimage)
                } else {
                    this.popToast("error", success.settings.message);
                    this.submittedProfile = false;
                }
            });
          }
    onUpdatePartnerProfile() {
        console.log("bhumika is grate======onUpdatePartnerProfile=======")
        console.log(this.data);
        console.log(this.datapi);
        if (this.data) {
            var adddata = Object.assign(this.data, this.datapi, this.dataImage)
            console.log("bhumika is grate 1", adddata)

        // } else if( this.datapi && this.data){
        //     var adddata = Object.assign(this.data, this.datapi, this.dataImage)
        //     console.log("bhumika is grate 2", adddata)

        // } else {
        //   var adddata = Object.assign(this.user, this.datapi, this.dataImage)
        //   console.log("bhumika is grate 3", adddata)
        }
        console.log("bhumika is grate", adddata)
        this.submittedProfile = true;
        this
            .commonService
            .callApi("updatePartnerProfile", adddata, 'put', '', true, true)
            .then(success => {
                this.spinner.hide();
                if (success.settings.status === 1) {
                    this.popToast("success", success.settings.message);
                    // this.setToken("username", success.data.name) this.setToken("profileimage",
                    // success.data.profileimage)
                } else {
                    this.popToast("error", success.settings.message);
                    this.submittedProfile = false;
                }
            });
          }
    /*************************************************************/

    /*************************************************************/
    // Get users profile
    /*************************************************************/
    getUserProfile() {
        this.spinner.show();
        this.commonService.callApi("getUserProfile", '', "get", true).then(success => {
                this.spinner.hide();
                if (success.settings.status == 1) {
                    this.user = success.data;
                    console.log("this.user", this.user);
                    if (this.user.photo) {
                        if (this.user.photo.includes('/null')) {
                            this.user.photo = '';
                        }
                    }} else {
                    this.popToast("error", success.message);
                  }});
    }
    /*************************************************************/
    /*************************************************************/
    // Get Escrow profile
    /*************************************************************/
    // getEscrowProfile() {   this.spinner.show();   this.commonService
    // .callApi("getUserProfile", '', "get", true)     .then(success => {
    // this.spinner.hide();       if (success.settings.status == 1) {
    // this.user = success.data;         console.log("this.user", this.user);
    // if (this.user.photo) {           if
    // (this.user.profileimage.includes('/null')) {
    // this.user.profileimage = '';           }         }       } else {
    // this.popToast("error", success.message);       }     }); }
    /*************************************************************/
    /*************************************************************/
    // Get PM profile
    /*************************************************************/
    // getPMProfile() {   this.spinner.show();   this.commonService
    // .callApi("getUserProfile", '', "get", true)     .then(success => {
    // this.spinner.hide();       if (success.settings.status == 1) {
    // this.user = success.data;         console.log("this.user", this.user);
    // if (this.user.profileimage) {           if
    // (this.user.profileimage.includes('/null')) {
    // this.user.profileimage = '';           }         }       } else {
    // this.popToast("error", success.message);       }     }); }
    /*************************************************************/
    /*************************************************************/
    // Get users profile
    /*************************************************************/
    getPartnerProfile() {
        this.spinner.show();
        this.commonService.callApi("getPartnerProfile", '', "get", true).then(success => {
                this.spinner.hide();
                if (success.settings.status === 1) {
                    this.user = success.data;
                    console.log("this.user", this.user);
                    if (this.user.profileimage) {
                        if (this.user.profileimage.includes('/null')) {
                            this.user.profileimage = '';
                        }}} else {
                    this.popToast("error", success.message);
                }});
    }
    /*************************************************************/

    /*************************************************************/
    //  Submit change password
    /*************************************************************/
    user: any = {};
    userPassword: any = {}
    submittedPassword: Boolean;
    onChangePassword(form, data) {
        this.submittedPassword = true;
        if (form.valid && (data.confirmPassword === data.newPassword)) {
            this.spinner.show();
            const user = {
                oldpassword: data.currentPassword,
                newpassword: data.newPassword,
                userId: this.getToken("_id")
            };
            this.commonService.callApi("changePassword", user, "post").then(success => {
                    this.spinner.hide();
                    if (success.settings.success === 1) {
                        this.bsModalRef.hide();
                        this.popToast("success", success.settings.message);
                    } else {
                        this.popToast("error", success.settings.message);
                        this.submittedPassword = false;
                    }}, error => {});}}
    /*************************************************************/

}
