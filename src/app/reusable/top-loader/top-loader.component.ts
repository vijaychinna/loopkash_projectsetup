import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { Meta } from '@angular/platform-browser';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'app-top-loader',
  templateUrl: './top-loader.component.html',
  styles: []
})
export class TopLoaderComponent extends BaseComponent implements OnInit {
  public topLoaderContent:any;
  constructor(inj: Injector, private meta: Meta,private _sanitizer: DomSanitizer) {
    super(inj);
    if (this.getToken('toploader') && this.getToken('toploader') != 'false') {
      this.getTopLoaderPage();
    }else{
      this.popToast('error','You only have access to this page once you have 100 successful trades within 30days');
      this._location.back();
    }
  }

  ngOnInit() {

  }
  // *************************************************************************** //
  // Gets top loader page
  // *************************************************************************** //

  getTopLoaderPage() {
    this.spinner.show();
    this.commonService.callApi('templatedetails?templatecode=Toploader', {}, 'get').then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.topLoaderContent = success.data[0];
        this.topLoaderContent.content = this._sanitizer.bypassSecurityTrustHtml(this.topLoaderContent.content);
        this.meta.addTag({ name: "title", content:  this.topLoaderContent.metatitle});
        this.meta.addTag({ property: 'og:title', content: this.topLoaderContent.metatitle });
        this.meta.addTag({ property: 'og:type', content: 'website' });
      } else {
        this.popToast("error", success.message);
      }
    });
  }
  // *************************************************************************** //
}
