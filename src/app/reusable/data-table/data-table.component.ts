import { Component, OnInit, Injector, Input,Output, EventEmitter } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styles: []
})
export class DataTableComponent extends BaseComponent implements OnInit {

 data: any = {};
 total: any = 50;
public tempData:any=[];
  constructor(inj:Injector) {
  super(inj) 
   this.data = { page: 1, pageSize: 5 };
}

@Input() tableSetupData : object;
@Output() selectedRecipient = new EventEmitter();

  

  ngOnInit() {
    console.log("tableSetupData--------",this.tableSetupData);
  	this.getUserData(this.data);
  }

  /*************************************************************/
  // Collpase table row to see the details
  /*************************************************************/
  openDetails(index){
    this.tableData.data.forEach((o,idx) => {      
      if(index==idx)
        this.tableData.data[idx].isCollapsed=!this.tableData.data[idx].isCollapsed
      else
        this.tableData.data[idx].isCollapsed=false;
        // this.tableData.data[idx].isCollapsed=(index==idx)?this.tableData.data[idx].isCollapsed=!this.tableData.data[idx].isCollapsed:false;
    }); 
  }
  /*************************************************************/

  /*************************************************************/
  // Get Table data form api
  /*************************************************************/
  getUserData(data){

  	this.http.get('http://localhost:3000/posts?_limit='+data.pageSize+'&_page='+data.page).subscribe((res)=>{
  		// console.log("res",res);
      this.tableData.data=res;
      this.tableData.data.forEach(o => {
        o.isCollapsed=false;
      });
  		// this.total=res.length
  	})
  }
  /*************************************************************/

  /*************************************************************/
  // Sort Table data
  /*************************************************************/
  sortUserData(data){

  	this.http.get('http://localhost:3000/posts?_sort='+data.sortField+'&_order='+(data.sortOrder.toUpperCase())+'&_limit='+data.pageSize+'&_page='+data.page).subscribe((res)=>{

  		console.log("res",res);
  		this.tableData.data=res;
  		// this.total=res.length
  	})
  }
  /*************************************************************/

  tableData = {
    cols: ['id','author','title'],
    data: this.tempData
  };
  studentArr: Array<any> = [];
  studentPresentArr: Array<any> = [];
  pageSizeValue = 5; // Number of items per page to show.
  // *************************************************************//
   // Custom sorting in table
  // *************************************************************//
 
  public sortDirection: any;
  public sortField = {};
  onSorted(e) {
    this.sortField = {
      sortField: e.sortColumn,
      direction: e.sortDirection,
      objectName: e.objectName
    };
    // if (e.sortDirection === "desc") {
    //   this.sortDirection = -1;
    // } else {
    //   this.sortDirection = 1;
    // }
    this.data.sortField = e.sortColumn;
    this.data.sortOrder = e.sortDirection;

    this.sortUserData(this.data);
  }
  // *************************************************************//

  /***************************************************************/
  // Pagination for load listing //
  /***************************************************************/
  public currentPage = 1;
  public showBoundaryLinks = true;
  public rangeList = [2, 5, 10, 25, 100];
  public : any;
  pageChanged(e) {
    this.data.page = e.page;
    this.pageSizeValue = e.itemsPerPage;
    this.getUserData(this.data);
  }
  rangeChanged(e) {
    this.data.pageSize = e;
    this.data.page = 1;
   this.getUserData(this.data);
  }
  /***************************************************************/



}
