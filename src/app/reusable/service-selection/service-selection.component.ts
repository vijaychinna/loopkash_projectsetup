import { Component, OnInit, Injector,Output, EventEmitter } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-service-selection',
  templateUrl: './service-selection.component.html',
  styles: []
})
export class ServiceSelectionComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  servicesList=[{id:1, value:"Divorce Certificate",selected:false},
                {id:2, value:"Marriage Certificate",selected:false},
                {id:3, value:"School transacript",selected:false},
                {id:4, value:"Birth Certificate",selected:false},
                {id:5, value:"Immigration",selected:false},
                {id:6, value:"business Registration",selected:false}]
  selectedServiceList=[];
  @Output() selectedServices = new EventEmitter();

  ngOnInit() {
  }

  selectServices(obj,index){
    if (this.selectedServiceList.length) {
      if (this.selectedServiceList.find(o => o.id == obj.id)) {
        let index = this.selectedServiceList.findIndex(o => o.id == obj.id)
        this.selectedServiceList.splice(index, 1);
        let index1 = this.servicesList.findIndex(o => o.id == obj.id)
        this.servicesList[index1].selected=false;
        // this.servicesList.splice(index1, 1);
      }
      else {
        this.selectedServiceList.push(obj);
      }
    }
    else {
      this.selectedServiceList.push(obj);
    }
    this.selectedServices.emit(this.selectedServiceList);
    
  }

  

}
