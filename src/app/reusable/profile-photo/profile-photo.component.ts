import { Component, OnInit , Input , Injector , Output , EventEmitter} from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {BaseComponent} from './../../common/commonComponent';

@Component({
  selector: 'app-profile-photo',
  templateUrl: './profile-photo.component.html',
  styles: []
})
export class ProfilePhotoComponent extends BaseComponent implements OnInit {
  @Input("user")user: any = {};
  @Output() valueChange = new EventEmitter();
  constructor(inj: Injector, public modalService: BsModalService) { super(inj); }
  bsModalRef: BsModalRef;
  fileObject:any;
  imageData:any;
  datavalofdates:any;
  ngOnInit() {
    this.user.photo= 'assets/images/NoProfile.png'
  }
   /*************************************************************/
  // Image Upload
  /*************************************************************/
  valueChanged() { // You can give any function name
    let users = {
      photo: this.user.photo,
      isSetUnavaliablePeriod: this.user.isSetUnavaliablePeriod,
      unavaliableStartDate:this.datavalofdates.unavaliableStartDate,
      unavaliableEndDate:this.datavalofdates.unavaliableEndDate
    }
    this.valueChange.emit(users);
console.log("this.userssssssssss", users);

  }
  /*************************************************************/
  // Image Upload
  /*************************************************************/
  previewImage(event, type) {
    // Reference to the DOM input element
    var input = event.target;
    // Ensure that you have a file before attempting to read it
    if (input.files && input.files[0]) {
      // create a new FileReader to read this image and convert to base64 format
      var reader = new FileReader();
      // Define a callback function to run, when FileReader finishes its job
      reader.onload = (e: any) => {
        // Note: arrow function used here, so that "this.imageData" refers to the imageData of Vue component
        // Read image as base64 and set to imageData
        // if(input.files[0].size<=1000)
        // {
        this.imageData = e.target.result;
        this.fileObject = input.files[0];
        console.log("this.fileObject", this.fileObject)
        // this.sizeError=false;
        // }
        // this.sizeError=true;
      }
      // Start the reader job - read file as a data url (base64 format)
      reader.readAsDataURL(input.files[0]);
    }
  }
  /*************************************************************/
// API for upload image
submitImage() {
  console.log(this.fileObject);
  var fd = new FormData();
  fd.append('file', this.fileObject)
  switch (this.getToken('profileRole')) {
    case "USR":
      this.spinner.hide();
      fd.append('fileFolder', 'userProfilePic')
      break;
    case "PTR":
      this.spinner.hide();
      fd.append('fileFolder', 'partnerProfilePic')
      break;

    }

  console.log(fd);
  this.commonService.callApi('fileUpload', fd, 'post', '', true, false).then(success => {
          if (success.settings.status === 1) {
              // this.admin.photo = success.data.filepath;
              // console.log(this.admin.photo);
              this.popToast('success', success.settings.message)
              this.user = success.data
              this.user.photo = success.data.filePath
              this.valueChange.emit(this.user);
          } else {
              this.popToast('error', success.settings.message)
          }
      })

}
  /*************************************************************/
// API for remove image
removeImage() {
  this.user.photo= 'assets/images/NoProfile.png'
}
displaydate(valofdates){
console.log(valofdates)
this.datavalofdates = valofdates;
}
}
