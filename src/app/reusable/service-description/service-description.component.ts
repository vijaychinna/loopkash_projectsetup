import { Component, OnInit, Injector,Input, Output,EventEmitter } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-service-description',
  templateUrl: './service-description.component.html',
  styles: []
})
export class ServiceDescriptionComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}
  @Input() isSubmitted;
  @Output() descData = new EventEmitter();

  ngOnInit() {
  }

  emitData(data){
    this.descData.emit(data);
  }

}
