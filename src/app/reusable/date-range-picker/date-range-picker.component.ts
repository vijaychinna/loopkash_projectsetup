import { Component, OnInit, Input , Output , EventEmitter} from '@angular/core';

@Component({
  selector: 'app-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styles: []
})
export class DateRangePickerComponent implements OnInit {
  @Input("user")user: any = {};
  @Output() valueChange = new EventEmitter();
 
  
  constructor() { }

  ngOnInit() {
  }
  valueChanged(){
     let users={
      unavaliableStartDate:this.user.unavaliableStartDate,
      unavaliableEndDate:this.user.unavaliableEndDate
    }
    this.valueChange.emit(users);
console.log("this.userssssssssss", users);
  }
}
