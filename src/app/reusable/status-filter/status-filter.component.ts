import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-status-filter',
  templateUrl: './status-filter.component.html',
  styles: []
})
export class StatusFilterComponent implements OnInit {

  constructor() { }
  selectedStatus:any;
  statusList = [{name:'All',id:1},{name:'Pending',id:2},{name:'Completed',id:3}];


  ngOnInit() {
    // this.selectedStatus=this.statusList[0];
  }

  ngAfterViewInit() {
    setTimeout(()=>{
      this.selectedStatus=this.statusList[0].id;

    },10)
    console.log(this.selectedStatus,"this.selectedStatus");
  }

}
