import { Component, OnInit, Injector , Input , Output, EventEmitter} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';


@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styles: []
})
export class AddressComponent extends BaseComponent implements OnInit {
  @Input("user")user: any = {};
  @Output() valueChange = new EventEmitter();
  constructor(inj: Injector) { super(inj) }
  ngOnInit() {
  }
  valueChanged() { // You can give any function name
    this.user = {
      address: this.user.address,
      city: this.user.city,
      state: this.user.state,
      country: this.user.country
    }
console.log("this.userssssssssss", this.user);
    this.valueChange.emit(this.user);
}

}
