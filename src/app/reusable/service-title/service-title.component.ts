import { Component, OnInit, Injector,Input, Output,EventEmitter } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-service-title',
  templateUrl: './service-title.component.html',
  styles: []
})
export class ServiceTitleComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}
  @Input() isSubmitted;
  @Output() titleData = new EventEmitter();

  ngOnInit() {
  }

  emitData(data){
    this.titleData.emit(data);
  }

}
