import { Component, Input, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
    selector : 'breadcrumb',
    template : `
    <div  aria-label="breadcrumb" >
        <ol class="breadcrumb">
        <li *ngFor="let breadcrumb of breadcrumbs" class="breadcrumb-item" >
        <a  *ngIf="breadcrumb.url; else elseBlock" href="" [routerLink]="[breadcrumb.url]" >{{breadcrumb.localeKey | translate}}</a>
        <ng-template #elseBlock>
        <a class="breadcrumb-item active" aria-current="page">{{breadcrumb.localeKey | translate}}</a>
        </ng-template>
        </li>
    </ol>
    </div>
    `
})
export class BreadcrumbComponent  extends BaseComponent implements OnInit {
    @Input()
    breadcrumbs: object;

    constructor(inj: Injector) {super(inj) ;}
}

