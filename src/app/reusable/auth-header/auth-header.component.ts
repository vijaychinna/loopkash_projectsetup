import { Component, OnInit, TemplateRef, Injector, Input, AfterViewInit, HostListener } from '@angular/core';
// import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseComponent } from './../../common/commonComponent'
import { Body } from '@angular/http/src/body';
import { NavigationEnd } from "@angular/router";
import { DebugContext } from '@angular/core/src/view';
declare var jQuery: any;
declare var $: any;
@Component({
  selector: "app-auth-header",
  templateUrl: "./auth-header.component.html",
  styleUrls: ["./auth-header.component.css"]
})
export class AuthHeaderComponent extends BaseComponent implements OnInit {
  modalRefChangePassModal: BsModalRef;
  public userId;
  public routeUrl;
  constructor(inj: Injector) {
    super(inj);
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // }
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
      }
    })
    this.userId = this.getToken('_id');

    // this.commonService.socketEmit('dataSend', { userId: this.userId });
    // this.commonService.socketEmit('dataConnection', { userId: this.userId });
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {

        $('body').removeClass('body-hidden');
        $(".nav-toggle").removeClass("active");
        $("header nav").removeClass("open");

        // this.commonService.socketEmit('dataSend', { userId: this.userId });

      }
    })
  }
  data: any = {};

  ngOnInit() {
    this.data.userId = this.getToken("_id");
    this.data.page = 1;
    this.data.pagesize = 4;
    this.commonService.startSocket();
    this.getCurrentStatus();
    this.commonService.socketEmit('dataConnection', { userId: this.userId });
    this.getSocketDataOrder();

  }
  ngAfterViewInit() {

  }
  getCurrentStatus() {
    this.spinner.show()
    this.commonService.callApi('getUserCurrentInfo', {userId:this.userId}, 'post').then(success => {
      this.spinner.hide()
      if (success.settings.success === 1) {
        this.setToken("isAdminStatus", success.data.isAdminStatus);
        this.setToken("isWithdrawStatus", success.data.isWithdrawStatus);
        this.setToken("btc", success.data.totalbtc);
        this.setToken("naira", success.data.totalnaira);
        this.setToken("notificationCount", success.data.unreadCount);
      } else {
        this.popToast("error", success.settings.message);
      }
    });
  }
  notificationCount: any = 0;
  ngDoCheck() {
    this.notificationCount = this.getToken('notificationCount')
  }
  toggle() {
    $(".nav-toggle").toggleClass("active");
    $("header nav").toggleClass("open");
    if ($(".nav-toggle").hasClass('active')) {
      $('body').addClass('body-hidden');
    } else {
      $('body').removeClass('body-hidden');
    }
  }
  // *************************************************************************** //
  // *************************************************************************** //
  // Get data from socket
  // *************************************************************************** //
  notificationOrderData;
  notificationWithdrawalData;
  getSocketDataOrder() {
    this.commonService.socketListenOrder().subscribe(data => {
    })
  }
  // *************************************************************************** //
  notificationData;
  getNotificationList() {
    this.spinner.show();
    this.commonService.callApi('getNotificatonListing', this.data, 'post').then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.notificationData = success.data;
        this.setToken('notificationCount', success.settings.unreadCount);
      } else {
        if (success.settings.message === "No trancations history") {

        } else {
          this.popToast("error", success.settings.message);
        }
      }
    });
  }
  redirect(notification) {
    if (notification.notificationType == "withdraw_request") {
      this.router.navigate(['/user/withdrawal-history'])
    }
    if (notification.notificationType == "order_status") {
      this.router.navigate(['/user/transaction-history/' + notification.tradeId + ''])
    }
    if (notification.notificationType == "btc_trade_status") {
      this.router.navigate(['/user/trade-history/' + notification.tradeId + ''])
    }

  }
  // @HostListener('window:beforeunload', ['$event'])
  // unloadHandler(event) {

  //   event.preventDefault();
  //   // window.document.forms[0].submit();
  //   return false;
  // }
}
