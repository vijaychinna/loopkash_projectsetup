import { Component, OnInit, Injector , Input } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styles: []
})
export class ChangePasswordComponent extends BaseComponent implements OnInit {
  @Input("user")user: any = {};

  constructor(inj: Injector) { super(inj) }
  public submitted: Boolean = false;

  ngOnInit() {
  }
  changePassword(form) {
console.log("user=======", this.user)
    this.user = {
      oldPassword: form.value.oldPassword,
      newPassword: form.value.newPassword,
      userId: this.user._id
    }
    this.submitted = true;
    if (form.valid) {
        console.log("Password Form : ", form.value)
        if (form.value.newPassword !== form.value.confirmPassword) {
            this.popToast('error', "New password & Confirm password not matched")
            form.reset();
            this.submitted = false;
            return 0;
        }
        console.log("user$$$$$", this.user)
        this
            .commonService
            .callApi('changePassword', this.user , 'put', '', true, true)
            .then(success => {
                if (success.settings.status == 1) {
                    this.popToast('success', success.settings.message)
                } else {
                    this.popToast('error', success.settings.message)
                }
            })
    }

}
}
