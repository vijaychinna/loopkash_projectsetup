import { Component, OnInit ,Injector,Input} from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styles: []
})
export class SocialMediaComponent  extends BaseComponent implements OnInit {

  constructor(inj: Injector, private socialAuthService: AuthService) {
    super(inj);
  }

  @Input() from:any;

  ngOnInit() {
  }

  private socialData;
  private dataToRegister:any={};
  userRole:any;
  // private queryParams;
  public socialSignIn(socialPlatform: string,template) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then((userData) => {
      this.socialData = userData;
      console.log(this.socialData);
      if( this.socialData){
        // if (socialPlatformProvider == 'facebook') {
          this.dataToRegister = {
                "socialToken":this.socialData.token,
                "socialType":this.socialData.provider,
                "emailId":this.socialData.email,             
                "firstName":this.socialData.name.split(' ')[0],
                "lastName":this.socialData.name.split(' ')[1],
                "photo":this.socialData.image
            }
          // }
          if(this.from=='login'){
            this.signIn(this.socialData.email)
          }else{
            this.bsModalRef=this.modalService.show(template,{backdrop:'static'});
          }
      }
    },(error)=>{
      console.log(error,"Social media login error");      
    });
  }

  signIn(email){
    this.spinner.show();
    this.commonService.callApi("socialLogin", {emailId:email}, "post", '', false,true).then(success => {
      console.log("success",success);      
      
      if (success.settings.status === 1) {
        this.popToast("success", success.settings.message)
        this.setToken('accessToken', success.data.token);
        this.setToken('profileRole', success.data.role);
         switch (success.data.role) {
          case "SP":
            this.spinner.hide();
            this.router.navigate(['/service-provider/dashboard'])
            break;
          case "ESP":
            this.spinner.hide();
            this.router.navigate(['/escrow-service-provider/dashboard'])
            break;
          case "USR":
            this.spinner.hide();
            this.router.navigate([this.UrlConstants.CREATE_OFFER])
            break;
          case "PTR":
            this.spinner.hide();
            this.router.navigate([this.UrlConstants.LEDGER])
            break;
          case "PM":
            this.spinner.hide();
            this.router.navigate(['/project-manager/dashboard'])
            break;
          }
      } else {
        this.popToast("error", success.settings.message)
      }
    })
  }

  register(userRole){   
    this.dataToRegister.role=userRole;
    this.bsModalRef.hide();
    this.spinner.show();
      this.commonService.callApi('socialSignUp', this.dataToRegister, 'post', '', false, true).then(success => {
        console.log(" successsss", success);
          if (success.settings.status == 1) {
            this.spinner.hide();
            this.popToast('success', success.settings.message);

            // NEED DATA FROM BACKEND TO AUTO LOGIN SAME AS LOGIN API
            // this.setToken('userdetail', JSON.stringify(success.data[0]));
            // this.commonService.loginRequest.emit(true);
            // this.checkLoginStatus();
            // this.signOut();
            // this.router.navigateByUrl("/main/dashboard");
          } else {            
            this.spinner.hide();
            this.popToast('error', success.message);
          }
        });
  }

}

