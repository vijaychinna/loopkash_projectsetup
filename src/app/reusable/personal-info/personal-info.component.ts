import { Component, OnInit, Injector , Input , Output, EventEmitter} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styles: []
})
export class PersonalInfoComponent extends BaseComponent implements OnInit {
  @Input("user")user: any = {};
  @Output() valueChange = new EventEmitter();
public isPartner:Boolean=false

  constructor(inj: Injector) { super(inj)
    switch (this.getToken('profileRole')) {
      case "SP":
        this.spinner.hide();
        // this.getServiceProviderProfile()
        break;
      case "ESP":
        this.spinner.hide();
        // this.getEscrowProfile()
        break;
      case "USR":
        this.spinner.hide();
        this.isPartner = false
        break;
      case "PTR":
        this.spinner.hide();
        this.isPartner = true

        break;
      case "PM":
        this.spinner.hide();
        // this.getPMProfile()
        break;
      }
  }

  ngOnInit() {


  }

  valueChanged() { // You can give any function name
    if(this.user.role === 'USR') {
      this.user = {
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        emailId: this.user.emailId,
        phoneNumber: this.user.phoneNumber,
        paypal: this.user.paypal,
        quickpay: this.user.quickpay,
        dob: this.user.dob
      }
      this.valueChange.emit(this.user);
      console.log("this.userssssssssss", this.user);
    }if(this.user.role === 'PTR') {
      this.user = {
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        screenName: this.user.screenName,
        phoneNumber: this.user.phoneNumber,
        paypal: this.user.paypal,
        quickpay: this.user.quickpay,
        dob: this.user.dob,
        accountManagerName: this.user.accountManagerName,
        currentBusiness: this.user.currentBusiness,
        companyLegalName: this.user.companyLegalName,
        managerEmail: this.user.managerEmail
        // isSetUnavaliablePeriod: this.user.isSetUnavaliablePeriod,
        // unavaliableStartDate:this.user.unavaliableStartDate,
        // unavaliableEndDate:this.user.unavaliableEndDate
      }
      this.valueChange.emit(this.user);
      console.log("this.userssssssssss partner", this.user);
    }
  }

}
