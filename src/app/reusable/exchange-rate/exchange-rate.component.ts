import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-exchange-rate',
  templateUrl: './exchange-rate.component.html',
  styles: []
})
export class ExchangeRateComponent extends BaseComponent implements OnInit {

   constructor(inj: Injector) {super(inj) ;}

  ngOnInit() {
  }

}
