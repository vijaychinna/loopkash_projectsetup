import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styles: []
})
export class FaqComponent extends BaseComponent implements OnInit {
  public FaqList = [];
  public faqListArray = [];
  public searchText: any;
  public list = [];
  constructor(inj: Injector) {
    super(inj);
    this.getFAQList();
  }

  ngOnInit() {
  }
  /*************************************************************/
  // Get faq list
  /*************************************************************/
  getFAQList() {
    this.spinner.show();
    this.commonService
      .callApi("categoryWiseFaqList", {}, "get")
      .then(success => {
        this.spinner.hide();
        if (success.settings.success === 1) {
          let self = this;
          success.data.forEach(function (data, index) {
            self.faqListArray.push({ _id: data._id, faqInfo: data.faqInfo, createdAt: data.createdAt, categoryFaqName: data.categoryFaqName, uniqueId : data.uniqueId })
            self.FaqList.push({ _id: data._id, faqInfo: data.faqInfo, createdAt: data.createdAt, categoryFaqName: data.categoryFaqName, uniqueId : data.uniqueId });
            self.list.push({ _id: data._id, faqInfo: data.faqInfo, createdAt: data.createdAt, categoryFaqName: data.categoryFaqName, uniqueId : data.uniqueId });
          })
          this.FaqList.forEach(function (data, i) {
            if (data.faqInfo.length > 4) {
              data.faqInfo = data.faqInfo.slice(0, 4);
            }
          });
        } else {
          this.popToast("error", success.message);
        }
      });
  }
  /*************************************************************/
  faqCount:any = [];
  searchFAQ() {
    let self = this;
    this.faqCount=[];
    if (this.searchText) {

      var searchList = this.list;
      this.FaqList = [];
      searchList.forEach(function (data, index) {
        data.faqInfo = data.faqInfo.filter(value => value.title.toLowerCase().includes(self.searchText.toLowerCase()));

        if (data.faqInfo.length > 0) {
          self.FaqList.push(searchList[index]);
        }
      })
      this.FaqList.forEach(function (data, i) {
        self.faqCount.push(data.faqInfo.length)
      });
      this.FaqList.forEach(function (data, i) {
        if (data.faqInfo.length > 4) {
          data.faqInfo = data.faqInfo.slice(0, 4);
        }
      });
    } else {
      this.FaqList = [];
      this.list = [];
      this.faqListArray.forEach(function (data, index) {
        self.FaqList.push({ _id: data._id, faqInfo: data.faqInfo, createdAt: data.createdAt, categoryFaqName: data.categoryFaqName, uniqueId : data.uniqueId });
        self.list.push({ _id: data._id, faqInfo: data.faqInfo, createdAt: data.createdAt, categoryFaqName: data.categoryFaqName, uniqueId : data.uniqueId });
      })
      this.FaqList.forEach(function (data, i) {
        if (data.faqInfo.length > 4) {
          data.faqInfo = data.faqInfo.slice(0, 4);
        }
      });
    }
  }
}
