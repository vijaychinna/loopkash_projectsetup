import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import {SearchFilter} from './../../common/search-filter';
@Component({
  selector: 'app-faq-list',
  templateUrl: './faq-list.component.html',
  styles: []
})
export class FaqListComponent extends BaseComponent implements OnInit {
  public FaqList = [];
  public faqId;
  public searchText: any;
  public categoryName: any;
  public categoryId:any;
  constructor(inj: Injector) {
    super(inj);
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.faqId = params['id'];
        this.getFAQList();
      } else {
        this.router.navigate(['/'])
      }
    })
  }

  ngOnInit() {
  }

  /*************************************************************/
  // Get faq list by category
  /*************************************************************/
  getFAQList() {
    this.spinner.show();
    this.commonService
      .callApi("categoryWiseFaqDetails?uniqueId=" + this.faqId + "", {}, "get")
      .then(success => {
        this.spinner.hide();
        if (success.settings.success === 1) {
          let self = this;
          if (success.data.length > 0) {
            this.categoryName = success.data[0].categoryFaqName;
            this.categoryId = success.data[0].uniqueId;
          }
          success.data.forEach(function(data,index){
            self.FaqList.push({title:data.title,_id:data._id, uniqueId: data.uniqueId});
          });
        } else {
          this.popToast("error", success.settings.message);
        }
      });
  }
}
