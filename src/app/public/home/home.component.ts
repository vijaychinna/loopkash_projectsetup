import { Component, OnInit, Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']

})
export class HomeComponent extends BaseComponent  implements OnInit {
	public payPalConfig?: PayPalConfig;

    ngOnInit(): void {
      this.initConfig();
    }

    private initConfig(): void {
      this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
        commit: true,
        client: {
          sandbox: 'AcLtdosRsGehU_o8DqZ2AR9DAsHCBw7MhCmv5s3Isddpo4ib4ljskXjuIdgBUz4DJRVXtLSmnHtfnPVX'
        },
        button: {
          label: 'paypal',
        },
        onPaymentComplete: (data, actions) => {
          console.log('OnPaymentComplete');
        },
        onCancel: (data, actions) => {
          console.log('OnCancel');
        },
        onError: (err) => {
          console.log('OnError');
        },
        transactions: [{
          amount: {
            currency: 'USD',
            total: 123
          }
        }]
      });
    }
	public FromCountry = ['Germany-Euro','United-Dollar','India - INR'];
	public DestinationCountry = ['Germany-Euro','United-Dollar','India - INR'];
  token: any;
  emailId: any;
	constructor(inj:Injector) {
  super(inj);
  console.log(this.router.url);
  this.token = this.router.parseUrl(this.router.url).queryParams["token"];
    console.log(this.token);
	}


  ngAfterViewInit() {


		if (this.token && this.isBrowser()) {
			this.verifyUser();
		}

	}

	//**********************************************************//
	//@Purpose : To animate elements on home page.
	//*******************************************************//

  verifyUser() {
    this.commonService.callApi('verifiedUser?token='+this.token, '', 'get', '', false, true).then(success => {
				if (success.settings.status === 1) {
						this.popToast('success', success.settings.message);
						setTimeout(()=>{
							this.router.navigate([this.UrlConstants.LOGIN]);
						},100)
				} else {
						this.popToast('error', success.settings.message);
				}
		},(error)=>{
			console.log("error",error);
		});
}
	//**********************************************************//
	carouselOptions = {
	    dots: true, nav: false, margin:0, responsive:
	    {
	      '0': {  items: 1},
	      '640': { items: 1},
	      '960' : {  items: 1}
	    },
  	}
  	public banners = [
	    {
	      "title": "Never Send Money Across Borders Again",
	      'description' : "and start accepting offers"
	    },
	    {
	      "title": "Never Send Money Across Borders Again",
	      "description":"and start accepting offers"
	    },
	    {
	      "title": "Never Send Money Across Borders Again",
	      "description" : "and start accepting offers"
	    }
  	]
}
