import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
@Component({
  selector: 'app-reset-password',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']

})
export class ResetPasswordComponent extends BaseComponent implements OnInit {
  public data: any = {};
  public submittedReg: Boolean = false;
  public token: any;
  public dataId: any;
  constructor(inj: Injector) {
    super(inj);
    console.log(this.router.url);
    this.token = this.router.parseUrl(this.router.url).queryParams["token"];
      console.log(this.token);
  }

  ngOnInit() {

  }
  resetPassword(form, data) {
    console.log(form , data);
    this.submittedReg = true;
    if (form.valid && (this.data.password === this.data.confirmPassword)) {
      let resetData = {
        token: this.token,
        password: this.data.password

      }
    console.log(resetData);
    this.spinner.show();
      this.commonService.callApi("resetPassword", resetData, "post", '', false , true).then(success => {
        this.spinner.hide();
        if (success.settings.status === 1) {
          this.submittedReg = false;
          this.popToast("success", success.settings.message)
          this.spinner.hide();
        } else {
          this.popToast("error", success.settings.message)
          this.spinner.hide();
        }
      })
    }
  }
}
