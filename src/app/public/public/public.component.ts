import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styles: []
})
export class PublicComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {
    super(inj);
  }

  ngOnInit() {
  }
  public isLogin:boolean = false;
  ngDoCheck(){
    if(this.getToken("accessToken")){
      this.isLogin = true;
    }else{
      this.isLogin = false;
    }
  }
}
