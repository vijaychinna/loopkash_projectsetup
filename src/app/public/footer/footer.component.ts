import { Component, OnInit ,Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {
    super(inj); }
  public currentYear :any = new Date().getFullYear();
  
  ngOnInit() {


  }
  
}
