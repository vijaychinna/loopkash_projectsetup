import { Component, OnInit, Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { Meta } from '@angular/platform-browser';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'app-about-us',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']

})
export class AboutUsComponent extends BaseComponent  implements OnInit {
  public aboutUsCotent:any={};
  public aboutusContent:any;
  constructor(inj:Injector,private meta: Meta,private _sanitizer: DomSanitizer) {
    super(inj);
    this.getAboutUsPage();
  }

  ngOnInit() {
  
  }
 // *************************************************************************** //
  // Get about us page
  // *************************************************************************** //

  getAboutUsPage() {
    this.spinner.show();
    this.commonService.callApi('templatedetails?templatecode=AboutUs', {}, 'get','',true).then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.aboutusContent = success.data[0];
        this.aboutusContent.content = this._sanitizer.bypassSecurityTrustHtml(this.aboutusContent.content);
        this.meta.addTag({ name: "title", content:  this.aboutusContent.metatitle});
        this.meta.addTag({ property: 'og:title', content: this.aboutusContent.metatitle });
        this.meta.addTag({ property: 'og:type', content: 'website' });
      } else {
        this.popToast("error", success.message);
      }
    });
  }
  // *************************************************************************** //
}