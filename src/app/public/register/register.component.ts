import { Component, OnInit, TemplateRef, Injector,ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseComponent } from './../../common/commonComponent';
import { ViewChild, ElementRef } from '@angular/core';
import { NavigationEnd } from "@angular/router";
// import {MatSlideToggleChange} from '@angular/material';
// import {MatPasswordStrengthComponent} from '@angular-material-extensions/password-strength';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class RegisterComponent extends BaseComponent implements OnInit{
  submittedReg: Boolean = false;
  constructor(inj: Injector) {
    super(inj);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
      }
    })

  }
  ngOnInit() {}

  onStrengthChanged(e){}

/*************************************************************/
// User Register
/*************************************************************/

  userId: any;
  onRegister(form, user) {
    if (!form.valid && (this.user.password === this.user.confirmPassword)) {
      this.spinner.show();
      user.emailId = user.emailId.toLowerCase();
      console.log("form & user", user)
      this.commonService.callApi("register", user, "post", '', false, true).then(success => {
        console.log("success",success);
        this.spinner.hide();
        if (success.settings.status === 1) {
          this.popToast("success", success.settings.message)
          this.router.navigate([this.UrlConstants.LOGIN]);
        } else {
          this.popToast("error", success.settings.message)
        }
      })
    } else {
       this.submittedReg = true;
    }
  }
  /*************************************************************/
}
