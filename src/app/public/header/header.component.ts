import { Component, OnInit, TemplateRef, Injector } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseComponent } from './../../common/commonComponent';
import { ViewChild, ElementRef } from '@angular/core';
import { NavigationEnd } from "@angular/router";
declare var jQuery: any;
declare var $: any;
//declare var windowSize:any;
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent extends BaseComponent implements OnInit {
  @ViewChild('payment') payment: TemplateRef<any>;

  user: any = {};
  public referralCode;
  public routeUrl;
  public lastName
  public firstName
  public  islogin: Boolean = false;
  constructor(inj: Injector) {
    super(inj);
    this.lastName = this.getToken("lastName");
    console.log("lastName", this.lastName)
    this.firstName = this.getToken("firstName");
    console.log("firstName", this.firstName)
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        setTimeout(()=>{this.routeUrl = event.urlAfterRedirects;
        console.log("this.routeUrl",this.routeUrl);
      }, 0);

      }
    })
  }

  onActivate() { }
  ngOnInit() {
      //var windowSize = $(window).width();
      this.islogin = false
        // this.routeUrl = this.router.url;
        //  console.log("this.routeUrl---",this.routeUrl,this.router);

  }
  ngAfterViewInit(){
    this.SlideNavigation();
  }

  // Nav Transform
  toggle() {
    $(".nav-toggle").toggleClass("active");
    //$("header nav").toggleClass("open");
    if ($(".nav-toggle").hasClass('active')) {
      $("body").addClass('body-hidden');
    } else {
      $("body").removeClass('body-hidden');
    }
  }
  navclose(){
    $("body").removeClass('body-hidden');
    $(".nav-toggle").removeClass("active");
  }
  removeOpen() {
    $("body").removeClass('body-hidden');
    $(".nav-toggle").removeClass("active");
  }
  SlideNavigation(){
    if ($(window).width() <= 993) {
          $(".dropdown-open > a").on("click", function(e) {
            console.log("e", e.target)
            //$(e.target)
            $(".submenu").slideUp(200);
            if ($(e.target).parent().hasClass("active")) {
              $(".dropdown-open").removeClass("active");
              $(e.target).parent().removeClass("active");
            } else {
              $(".dropdown-open").removeClass("active");
              $(e.target).next(".submenu").slideDown(200);
              $(e.target).parent().addClass("active");
            }
          });
      }
  }

  // Sticky Header on Scroll
  myScrollHandler() {
    const scroll = $(window).scrollTop();
    if (scroll >= 100) {
      $("header").addClass("fixed");
    } else {
      $("header").removeClass("fixed");
    }
  }



  /*************************************************************/
  // Forgot Password Start
  /*************************************************************/
  submittedForgot: Boolean;
  modalRefForgot: BsModalRef;
  openForgotModal(template: TemplateRef<any>) {
    // this.modalRefLogin.hide();
    this.modalRefForgot = this.modalService.show(template, {
      class: "modal-dialog-centered auth-modal"
    });
    this.user = {}
    this.submittedForgot = false;
  }
  onForgotPass(form, data) {
    this.submittedForgot = true;
    if (form.valid) {
      this.spinner.show();
      data.email = data.email.toLowerCase();
      this.commonService
        .callApi("forgotPassword", data, "post", "", true)
        .then(success => {
          this.spinner.hide();
          if (success.settings.success === 1) {
            this.modalRefForgot.hide();
            this.popToast("success", success.settings.message);
          } else {
            this.popToast("error", success.settings.message);
            this.submittedForgot = false;
          }
        });
    }
  }
  /*************************************************************/





  // *************************************************************************** //
  // Get the list of currency
  // *************************************************************************** //
  currencyList: Array<any> = []
  getCurrencyList() {
    this.spinner.show();
    this.commonService.callApi('currnencylisting', '', 'get').then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.currencyList = success.data;
      } else {
        this.popToast("error", success.message);
      }
    });
  }
  // *************************************************************************** //
  selectCurrency(data) {
    this.user = {
      currency: data
    }
  }
  /*************************************************************/
  /*************************************************************/
  // Get Balance Details
  /*************************************************************/
  getBalance(form, data) {

  }
  /*************************************************************/

  // *************************************************************************** //
  // Remove e,+,_
  // *************************************************************************** //
  removeText(event) {
    if (event.which !== 8 && event.which !== 0 && event.which < 48 || event.which > 57) {
      event.preventDefault();
    }

  }
}

