import { Component, OnInit,  Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent extends BaseComponent implements OnInit{

  constructor(inj: Injector) {
    super(inj);
  }

  ngOnInit() {
  }
  /*************************************************************/
  // User Login
  /*************************************************************/
  user: any = {};
  submittedLogin: Boolean=false;


  onLogin(form, user) {
    console.log("user", user)
    if (form.valid) {
      this.spinner.show();
        // this.setToken('accessToken',"142351234513461235613");
        // this.setToken('profileRole', user.userRole);
          // switch (user.userRole) {
          //   case "SP":
          //     this.spinner.hide();
          //     this.router.navigate(['/service-provider/dashboard'])
          //     break;
          //   case "ESP":
          //     this.spinner.hide();
          //     this.router.navigate(['/escrow-service-provider/dashboard'])
          //     break;
          //   case "USR":
          //     this.spinner.hide();
          //     this.router.navigate(['/user/create-offer'])
          //     break;
          //   case "PTR":
          //     this.spinner.hide();
          //     this.router.navigate(['/partner/ledger'])
          //     break;
          //   case "PM":
          //     this.spinner.hide();
          //     this.router.navigate(['/project-manager/dashboard'])
          //     break;
          //   }

      this.commonService.callApi("login", user, "post", '', false,true).then(success => {
        console.log("success",success);

        this.spinner.hide();
        if (success.settings.status === 1) {

          this.popToast("success", success.settings.message)
          this.setToken('accessToken', success.data.accessToken);
          this.setToken('profileRole', success.data.role);
          this.setToken('firstName', success.data.firstName);
          this.setToken('lastName', success.data.lastName);
           switch (success.data.role) {
            case "SP":
              this.spinner.hide();
              this.router.navigate(['/service-provider/dashboard'])
              break;
            case "ESP":
              this.spinner.hide();
              this.router.navigate(['/escrow-service-provider/dashboard'])
              break;
            case "USR":
              this.spinner.hide();
              this.router.navigate([this.UrlConstants.CREATE_OFFER])
              break;
            case "PTR":
              this.spinner.hide();
              this.router.navigate([this.UrlConstants.LEDGER])
              break;
            case "PM":
              this.spinner.hide();
              this.router.navigate(['/project-manager/dashboard'])
              break;
            }

        } else {
          this.popToast("error", success.settings.message)
        }
      })
    }
    else{
       this.submittedLogin = true;
    }
  }
  /*************************************************************/


}
