import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styles: []
})
export class ForgotPasswordComponent extends BaseComponent implements OnInit {

  user: any = {};
  submittedLogin: Boolean = false;

  constructor(inj: Injector) {
    super(inj); }

  ngOnInit() {
  }

  forgotPassword(form, data) {
    console.log(form, data);

  this.commonService.callApi('forgotPasswordMail', data, 'post', "", "", true)
  .then(success => {
      this.popToast("success", "Link Sent");
  }
)
 }



}
