import { Component, OnInit ,Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-faq-detail',
  templateUrl: './faq-detail.component.html',
  styles: []
})
export class FaqDetailComponent extends BaseComponent implements OnInit {
  public FaqDetail:any={};
  public faqId;
  public searchText: any;
  public categoryName: any;
  public categoryId:any;
  constructor(inj: Injector) {
    super(inj);
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.faqId = params['id'];
        this.getFAQDetail();
      } else {
        this.router.navigate(['/'])
      }
    })
  }

  ngOnInit() {
  }
  /*************************************************************/
  // Get faq detail
  /*************************************************************/
  getFAQDetail() {
    this.spinner.show();
    this.commonService
      .callApi("faqWebsiteDetail", {uniqueId:this.faqId}, "post")
      .then(success => {
        this.spinner.hide();
        if (success.settings.success === 1) {
          this.FaqDetail = success.data[0];
        } else {
          this.popToast("error", success.settings.message);
        }
      });
  }
}
