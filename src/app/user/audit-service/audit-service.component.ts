import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { IBreadcrumbs,ITableSetupData } from './../../common/interfaces';

@Component({
  selector: 'app-audit-service',
  templateUrl: './audit-service.component.html',
  styles: []
})
export class AuditServiceComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  breadcrumbs: IBreadcrumbs[];
  tableSetupData: ITableSetupData = {cols:[], isCheckbox: false,actions : null,isCollapsible:null };
  isSubmitted:boolean=false;

  ngOnInit() {

    this.setBreadcrumbs();
    this.setDTableInitialData();
  }

  setDTableInitialData(){
    let tempData=[
        {type : 'checkbox',colName:'',colFieldname:''},
        {type : 'text',colName:'business-name',colFieldname:'buss_name'},
        {type : 'text',colName:'business-type',colFieldname:'city'},
        {type : 'text',colName:'avg-price',colFieldname:'state_province'},
        {type : 'text',colName:'total-exp',colFieldname:'country'},
        // {type : 'select',colName:'match', colFieldname:'receiveOn'},
        {type : 'collapse',colName:'',colFieldname:''},
    ]
    this.tableSetupData.cols=tempData;
    this.tableSetupData.isCheckbox=true;
    this.tableSetupData.isCollapsible=true;
    console.log("this.tableSetupData",this.tableSetupData   );
  }

  public setBreadcrumbs() {
        this.breadcrumbs = [
            {localeKey: 'request-service', url: null},
            {localeKey: 'medical-service', url: null},
        ];
    }

    submitAuditServicePost(){
        
    }

}
