import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { IBreadcrumbs,ITableSetupData } from './../../common/interfaces';
import { AddRecipientComponent } from './../../modals/add-recipient/add-recipient.component';
import { TransactionSummaryComponent } from './../../modals/transaction-summary/transaction-summary.component';

@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.component.html',
  styles: []
})
export class CreateOfferComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  breadcrumbs: IBreadcrumbs[];
  tableSetupData: ITableSetupData = {cols:[], isCheckbox: false,actions : null };
  selectedRecipient:any;

  ngOnInit() {

  	//Translate column names before passing them to data table

  	 this.translateString(['admin_define.AssetsList', 'admin_define.AssetType']).then((res) => {
            if (res) {
            	// console.log("translated res--",res);
            }
        });

     this.setBreadcrumbs();
     this.setDTableInitialData();
  }

  setDTableInitialData(){
    let tempData=[
        {type : 'checkbox',colName:'',colFieldname:''},
        {type : 'text',colName:'Name',colFieldname:'name'},
        {type : 'text',colName:'City',colFieldname:'city'},
        {type : 'text',colName:'State/Province',colFieldname:'state_province'},
        {type : 'text',colName:'Country',colFieldname:'country'},
        {type : 'select',colName:'Receive On',  data: [ {name: 'Cellphone',code:'CP'},
                                                        {name: 'Paypal',code:'PP'},
                                                        {name: 'Quickpay',code:'QP'}],colFieldname:'receiveOn'
        },
        {type : 'text',colName:'Cellphone',colFieldname:'cellPhone'},
    ]
    this.tableSetupData.cols=tempData;
    this.tableSetupData.isCheckbox=true;
    console.log("this.tableSetupData",this.tableSetupData   );
  }

  public setBreadcrumbs() {
        this.breadcrumbs = [
            {localeKey: 'send-money', url: null},
            {localeKey: 'create-offer', url: null},
        ];
    }

  openAddRecipientModal(){
        this.bsModalRef = this.modalService.show(AddRecipientComponent,{class: 'modal-lg'/*,initialState: { data: this.CBdata }*/ });
    }

  openSummaryModal(){
        this.bsModalRef = this.modalService.show(TransactionSummaryComponent,{class: 'modal-lg'/*,initialState: { data: this.CBdata }*/ });
    }

}
