import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { IBreadcrumbs } from './../../common/interfaces';

@Component({
  selector: 'app-pending-agreements',
  templateUrl: './pending-agreements.component.html',
  styles: []
})
export class PendingAgreementsComponent extends BaseComponent implements OnInit {

   constructor(inj: Injector) {super(inj) ;}

    breadcrumbs: IBreadcrumbs[];

ngOnInit() {

  	//Translate column names before passing them to data table

  	 this.translateString(['admin_define.AssetsList', 'admin_define.AssetType']).then((res) => {
            if (res) {
            	console.log("translated res--",res);
            }
        });

     this.setBreadcrumbs();
  }

  public setBreadcrumbs() {
        this.breadcrumbs = [
            {localeKey: 'send-money', url: null},
            {localeKey: 'pending-agreements', url: null},
        ];
    }


}
