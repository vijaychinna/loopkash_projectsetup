import { Component, OnInit ,Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styles: []
})
export class NotificationListComponent extends BaseComponent implements OnInit {
  public userId;
  public data:any ={};
  maxSize: number = 5;
  pageSize: any = 10;
  pageNumber: number = 5;
  bigCurrentPage: number = 0;
  numPages: number = 0;
  notificationList: Array<any> = [];
  public totalPage:any;
  public totalData:any;
  public noDataFlag:boolean = false;
  constructor(inj: Injector) {
    super(inj);
    this.data.userId = this.getToken("_id");
    this.data.page = 1;
    this.data.pagesize = 12;
    this.data.searchText = '';
    this.getNotificationList();
  }

  ngOnInit() {
  }
  // *************************************************************************** //
  // Get the list of Notification list
  // *************************************************************************** //
  getNotificationList() {
    this.noDataFlag = false;
    this.spinner.show();
    this.commonService.callApi('getNotificatonListing', this.data, 'post').then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.notificationList = success.data;
        this.totalData = success.settings.total;
        this.totalPage = Math.ceil(this.totalData / this.data.pagesize);
      } else {
        if (success.settings.message === "No trancations history") {
          this.notificationList = [];
          this.totalPage = 0;
          this.totalData = 0;
          this.noDataFlag = true;
        }else{
          this.popToast("error", success.settings.message);
        }
      }
    });
  }
  // *************************************************************************** //

  // *************************************************************************** //
  // Page Change
  // *************************************************************************** //
  changePage(){
    this.data.page = this.bigCurrentPage;
    this.getNotificationList();
  }
  // *************************************************************************** //
}
