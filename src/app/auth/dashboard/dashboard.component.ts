import { Component, OnInit, Injector, DoCheck } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent extends BaseComponent implements OnInit, DoCheck {

  constructor(inj: Injector, private _sanitizer: DomSanitizer) {
    super(inj);
    this.getContent();
  }
  isAdminStatus;
  ngOnInit() {
  }
  ngDoCheck(){
    this.isAdminStatus = this.getToken('isAdminStatus')
  }
  showMessage(){
    this.popToast('error','Admin is currently offline. You can not start trade right now. Try after some time.')
  }

  public dashboardContent: any = {};
  getContent() {
    this.spinner.show();
    this.commonService.callApi('templatedetails?templatecode=dashboard', {}, 'get', '', true).then(success => {
      this.spinner.hide();
      if (success.settings.success === 1) {
        this.dashboardContent = success.data[0];
        this.dashboardContent.content = this._sanitizer.bypassSecurityTrustHtml(this.dashboardContent.content);
      } else {
        this.popToast("error", success.message);
      }
    });
  }
}
