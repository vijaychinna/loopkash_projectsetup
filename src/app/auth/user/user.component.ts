import { Component, OnInit, Injector} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: []
})

export class UserComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {
    super(inj);
  }
  ngOnInit() {
  }
}


