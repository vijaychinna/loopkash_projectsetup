import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule,NoopAnimationsModule} from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "./common/interceptor";

// Plugins
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { BsDatepickerModule, ModalModule,TabsModule, CollapseModule} from 'ngx-bootstrap';
import { BnDatatableModule } from './common/bn-datatable/bn-datatable.module'
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlModule } from 'ngx-owl-carousel';
import { MalihuScrollbarModule } from "ngx-malihu-scrollbar";
import { ImageCropperModule } from 'ngx-image-cropper';
import {ToasterModule} from 'angular2-toaster';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPayPalModule } from 'ngx-paypal';
// Common
import { BaseComponent } from './common/commonComponent';
import { CommonService } from './common/common.service';
import {SocketDataService} from './common/socketService';
import { CanLoginActivate, CanAuthActivate ,CanAuthActivateUser,CanHomeActivate, CanAuthActivatePartner, CanAuthActivateSP} from './common/auth.gaurd';
import { ErrorMessages } from './common/errorMessages';
import {SearchFilter} from './common/search-filter';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {GooglePlacesDirective} from './common/directives/google-places.directive';

// import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength'
// import {
//   MatButtonModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRippleModule
// } from '@angular/material';
import { AppRoutingModule, AppRoutingComponents, AppEntryComponents } from './app-routing.module';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http,'assets/i18n/', '.json');
}
// Configs
export function getAuthServiceConfigs() {
let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("253955838616828")//indianic account
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("197910794552-bvkjngqdq4pjfea8458nc3t81tem1rjh.apps.googleusercontent.com") //Indianic account
      },
        // {
        //   id: LinkedinLoginProvider.PROVIDER_ID,
        //   provider: new LinkedinLoginProvider("1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com")
        // },
    ]
);
return config;
}

@NgModule({
  declarations: [
    AppComponent,
    AppRoutingComponents,
    BaseComponent,
    CanLoginActivate,
    CanAuthActivate,
    SearchFilter,
    CanAuthActivateUser,
    CanAuthActivatePartner,
    CanAuthActivateSP,
    CanHomeActivate
  ],
  imports: [
    // NgxSocialShareModule,
    GooglePlaceModule,
    ImageCropperModule,
    MalihuScrollbarModule.forRoot(),
    NgxSpinnerModule,
    BnDatatableModule,
    NoopAnimationsModule,
    SocialLoginModule,
    NgxPayPalModule,
    // MatPasswordStrengthModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    CollapseModule.forRoot(),
    NgSelectModule,
    OwlModule,
    BsDatepickerModule.forRoot(),
    LoadingBarHttpClientModule,
    FormsModule,
    BrowserModule.withServerTransition({ appId: "my-app" }),
    HttpClientModule,
    ToasterModule.forRoot(),
    BrowserTransferStateModule,
    AppRoutingModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  //   MatButtonModule,
  // MatFormFieldModule,
  // MatInputModule,
  // MatRippleModule
    // RouterModule.forRoot(, {onSameUrlNavigation: "reload"})
  ],
  exports: [
    // MatButtonModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatRippleModule,
  ],
  providers: [
    CanLoginActivate,
    CanAuthActivate,
    CanAuthActivateUser,
    CanAuthActivatePartner,
    CanAuthActivateSP,
    CanHomeActivate,
    CommonService,
    SocketDataService,
    ErrorMessages,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    GooglePlacesDirective,
    BsModalService,
    BsModalRef,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  entryComponents : AppEntryComponents,
  bootstrap: [AppComponent]
})
export class AppModule {}


