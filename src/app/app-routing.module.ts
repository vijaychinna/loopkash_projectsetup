import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { DashboardComponent } from './auth/dashboard/dashboard.component';
import { ProfileComponent } from './reusable/profile/profile.component';
import { ChangePasswordComponent } from './reusable/change-password/change-password.component';
import { NotificationListComponent } from './auth/notification-list/notification-list.component';
import { UserComponent } from './auth/user/user.component';
import { PublicComponent } from './public/public/public.component';
import { RegisterComponent } from './public/register/register.component';
import { LoginComponent } from './public/login/login.component';
import { HomeComponent } from './public/home/home.component';
import { HeaderComponent } from './public/header/header.component';
import { FooterComponent } from './public/footer/footer.component';
import { ResetPasswordComponent } from './public/resetpassword/resetpassword.component';
import { ForgotPasswordComponent } from './public/forgot-password/forgot-password.component';
import { FaqComponent } from './cms/faq/faq.component';
import { TermsConditionsComponent } from './cms/terms-conditions/terms-conditions.component';
import { AboutusComponent } from './cms/aboutus/aboutus.component';
import { ContactusComponent } from './cms/contactus/contactus.component';
import { ServicesComponent } from './cms/services/services.component';

//DIRECTIVES
import {GooglePlacesDirective} from './common/directives/google-places.directive';

// Auth pages
import { AuthHeaderComponent } from './reusable/auth-header/auth-header.component';

import { CanLoginActivate, CanAuthActivate, CanAuthActivateUser,CanHomeActivate, CanAuthActivatePartner, CanAuthActivateSP } from './common/auth.gaurd';
import { SocialMediaComponent } from './reusable/social-media/social-media.component';
import { CreateOfferComponent } from './user/create-offer/create-offer.component';
import { MarketplaceComponent } from './user/marketplace/marketplace.component';
import { PendingAgreementsComponent } from './user/pending-agreements/pending-agreements.component';
import { MedicalServiceComponent } from './user/medical-service/medical-service.component';
import { AuditServiceComponent } from './user/audit-service/audit-service.component';
import { LegalServiceComponent } from './user/legal-service/legal-service.component';
import { EscrowServiceComponent } from './user/escrow-service/escrow-service.component';
import { BuyersPortalComponent } from './user/buyers-portal/buyers-portal.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { EmptyComponent } from './public/empty/empty.component';
import { DataTableComponent } from './reusable/data-table/data-table.component';
import { BreadcrumbComponent } from './reusable/breadcrumb/breadcrumb.component';
import { ExchangeRateComponent } from './reusable/exchange-rate/exchange-rate.component';
import { AddRecipientComponent } from './modals/add-recipient/add-recipient.component';
import { TransactionSummaryComponent } from './modals/transaction-summary/transaction-summary.component';
import { ServiceProviderDashboardComponent } from './service-provider/sp-dashboard/sp-dashboard.component';
import { TransactionHistoryComponent } from './service-provider/transaction-history/transaction-history.component';
import { PartnerLedgerComponent } from './partner/partner-ledger/partner-ledger.component';
import { MyActivityComponent } from './partner/my-activity/my-activity.component';
import { StatusFilterComponent } from './reusable/status-filter/status-filter.component';
import { DateRangePickerComponent } from './reusable/date-range-picker/date-range-picker.component';
import { ProfilePhotoComponent } from './reusable/profile-photo/profile-photo.component';
import { PersonalInfoComponent } from './reusable/personal-info/personal-info.component';
import { AddressComponent } from './reusable/address/address.component';
import { UserServicesComponent } from './reusable/user-services/user-services.component';
import { ServiceTitleComponent } from './reusable/service-title/service-title.component';
import { ServiceSelectionComponent } from './reusable/service-selection/service-selection.component';
import { ServiceDescriptionComponent } from './reusable/service-description/service-description.component';

const routes: Routes = [
      { path: "", redirectTo: 'home', pathMatch: "full" },
      { path: "reset-password", component: ResetPasswordComponent, pathMatch: "full", canActivate: [CanLoginActivate] },
      { path: "home", component: HomeComponent, pathMatch: "full"},

      { path: "empty", component: EmptyComponent, pathMatch: "full", canActivate: [CanHomeActivate] },
      {
        path: "public", component: PublicComponent, children: [
          { path: "login", component: LoginComponent, pathMatch: "full", canActivate: [CanLoginActivate]  },
          { path: "register", component: RegisterComponent, pathMatch: "full", canActivate: [CanLoginActivate]  },
          { path: "forgot-password", component: ForgotPasswordComponent, pathMatch: "full", canActivate: [CanLoginActivate]  },
          { path: "reset-password", component: ResetPasswordComponent, pathMatch: "full", canActivate: [CanLoginActivate] },
          { path: "my-profile", component: ProfileComponent, pathMatch: "full", canActivate: [CanAuthActivate]  },
        ]
      },
      {
        path: "user", component: PublicComponent, canActivate:[CanAuthActivateUser],children: [
          { path: "dashboard", component: DashboardComponent, pathMatch: "full" },
          { path: "create-offer", component: CreateOfferComponent, pathMatch: "full" },
          { path: "marketplace", component: MarketplaceComponent, pathMatch: "full" },
          { path: "agreements", component: PendingAgreementsComponent, pathMatch: "full" },
          { path: "legal-service", component: LegalServiceComponent, pathMatch: "full" },
          { path: "audit-service", component: AuditServiceComponent, pathMatch: "full" },
          { path: "medical-service", component: MedicalServiceComponent, pathMatch: "full" },
          { path: "escrow-service", component: EscrowServiceComponent, pathMatch: "full" },
          { path: "buyers", component: BuyersPortalComponent, pathMatch: "full" },
        ]
      },
      {
        path: "partner", component: PublicComponent, canActivate:[CanAuthActivatePartner],children: [
          { path: "ledger", component: PartnerLedgerComponent, pathMatch: "full" },
          { path: "my-activity", component: MyActivityComponent, pathMatch: "full" },
        ]
      },
      {
        path: "service-provider", component: PublicComponent, canActivate:[CanAuthActivateSP],children: [
          { path: "dashboard", component: ServiceProviderDashboardComponent, pathMatch: "full" },
           { path: "transaction-history", component: TransactionHistoryComponent, pathMatch: "full" },
        ]
      },
      {
        path: "cms", component: PublicComponent, children: [
          { path: "faq", component: FaqComponent, pathMatch: "full" },
          { path: "terms-conditions", component: TermsConditionsComponent, pathMatch: "full" },
          { path: "aboutus", component: AboutusComponent, pathMatch: "full" },
          { path: "contactus", component: ContactusComponent, pathMatch: "full" },
          { path: "services", component: ServicesComponent, pathMatch: "full" },
        ]
      },
      { path: "**", redirectTo: "empty", pathMatch: "full" }
    ]

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: "reload"})],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }

export const AppEntryComponents=[
  AddRecipientComponent,
  TransactionSummaryComponent
]

export const AppRoutingComponents=[
  HomeComponent,
  HeaderComponent,
  FooterComponent,
  AuthHeaderComponent,
  DashboardComponent,
  ProfileComponent,
  ChangePasswordComponent,
  FaqComponent,
  NotificationListComponent,
  UserComponent,
  PublicComponent,
  ResetPasswordComponent,
  RegisterComponent,
  LoginComponent,
  SocialMediaComponent,
  CreateOfferComponent,
  MarketplaceComponent,
  PendingAgreementsComponent,
  MedicalServiceComponent,
  AuditServiceComponent,
  LegalServiceComponent,
  EscrowServiceComponent,
  BuyersPortalComponent,
  DashboardComponent,
  EmptyComponent,
  DataTableComponent,
  ForgotPasswordComponent,
  FaqComponent,
  TermsConditionsComponent,
  AboutusComponent,
  ContactusComponent,
  ServicesComponent,
  BreadcrumbComponent,
  ExchangeRateComponent,
  AddRecipientComponent,
  TransactionSummaryComponent,
  GooglePlacesDirective,
  ServiceProviderDashboardComponent,
  TransactionHistoryComponent,
  PartnerLedgerComponent,
  MyActivityComponent,
  StatusFilterComponent,
  DateRangePickerComponent,
  ProfilePhotoComponent,
  PersonalInfoComponent,
  AddressComponent,
  UserServicesComponent,
  ServiceTitleComponent,
  ServiceSelectionComponent,
  ServiceDescriptionComponent
  ]

