import { Injectable, Injector } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { CommonService } from "./common.service";
import { Observable } from "rxjs/Observable";
import { BaseComponent } from './../common/commonComponent';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public inj: Injector) {
  }

  CommonService: CommonService;

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    this.CommonService = this.inj.get(CommonService);
    if (this.CommonService.getToken("accessToken")) {
      request = request.clone({
        setHeaders: {
          Authorization: `${this.CommonService.getToken(
            "accessToken"
          )}`
        }
      });
    } else {
      request = request.clone();
    }
    return next.handle(request);
  }
}
