export const URLConstants = {
 
    ABOUT_US            : '/cms/aboutus',
    AGREEMENTS          : '/user/agreements',
    AUDIT_SERV          : '/user/audit-service',
    CONTACT_US          : '/cms/contactus',
    CREATE_OFFER        : '/user/create-offer',
    ESCROW_SERV         : '/user/escrow-service',
    FAQ                 : '/cms/faq',    
    FORGOT_PASS         : '/public/forgot-password',
    HOME                : '/home',
    LEDGER              : '/partner/ledger',
    LEGAL_SERV          : '/user/legal-service',
    LOGIN               : '/public/login',
    MARKETPLACE         : '/user/marketplace',    
    MEDICAL_SERV        : '/user/medical-service',
    MY_ACTIVITY         : '/partner/my-activity',
    MY_PROFILE          : '/public/my-profile',
    REGISTER            : '/public/register',
    RESET_PASS          : '/public/reset-password',
    SERVICES            : '/cms/services',
    USER_DASH           : '/user/dashboard',
    USER_PROFILE        : '/user/my-profile',
    TERMS               : '/cms/terms-conditions',
    
   
};
