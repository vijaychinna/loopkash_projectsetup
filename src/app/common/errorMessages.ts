import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { messages } from './errorMessagesData';
@Injectable()
export class ErrorMessages {
  public MSG = (<any>messages)

  constructor() { }
  getError(field, error) {
    let message = '';
    if (error) {
      if (error.required) {
        const required = this.MSG.ERROR.REQUIRED;
        switch (field) {
          case "username":
            {
              message = required.Username;
            }
            break;
          case "name":
            {
              message = required.Name;
            }
            break;
          case "comment":
            {
              message = required.Comment;
            }
            break;
          case "email":
            {
              message = required.Email;
            }
            break;
          case "userRole":
            {
              message = required.UserRole;
            }
            break;

          case "mobile":
            {
              message = required.mobile;
            }
            break;
          case "password":
            {
              message = required.Password;
            }
            break;
          case "currentPassword":
            {
              message = required.currentPassword;
            }
            break;
          case "newPassword":
            {
              message = required.newPassword;
            }
            break;
          case "confirmPassword":
            {
              message = required.confirmPassword;
            }
            break;
          case "activity":
            {
              message = required.activity;
            }
            break;
          case "remark":
            {
              message = required.remark;
            }
            break;
          case "firstname":
            {
              message = required.firstname;
            }
            break;
          case "paypal":
            {
              message = required.paypal;
            }
            break;
          case "quickpay":
            {
              message = required.quickpay;
            }
            break;
          case "lastname":
            {
              message = required.lastname;
            }
            break;
          case "technology":      {
              message = required.technology;
            }
            break;
          case "category":
           {
              message = required.category;
            }
            break;
            
          case "subCategory":
            {
              message = required.subCategory;
            }
            break;
          case "currency":
            {
              message = required.currency;
            }
            break;
          case "cardAmount":
            {
              message = required.cardAmount;
            }
            break;
          case "cardAmount":
            {
              message = required.cardAmount;
            }
            break;
          case "city":
            {
              message = required.city;
            }
            break; 
          case "state":
            {
              message = required.state;
            }
            break; 
          case "country":
            {
              message = required.country;
            }
            break;
          case 'address': { message = required.Address } break;
          case 'accountname': { message = required.Accountname } break;
          case 'accountnumber': { message = required.Accountnumber } break;
          case 'bankId': { message = required.BankId } break;
        }
      } else if (error.pattern) {
        const pattern = this.MSG.ERROR.PATTERN;
        switch (field) {
          case 'email': { message = pattern.Email } break;
          case 'cardAmount': { message = pattern.cardAmount } break;
          case 'password': { message = pattern.Password } break;

        }
      } else if (error.minlength) {
        const minlength = this.MSG.ERROR.MINLENGTH;
        switch (field) {
          case 'mobile': { message = minlength.mobile } break;
        }
      }
      return message;
    } else {
      const required = this.MSG.ERROR.REQUIRED;
      switch (field) {
        case "category":
          {
            message = required.category;
          }
          break;
        case "subCategory":
          {
            message = required.subCategory;
          }
          break;
        case "currency":
          {
            message = required.currency;
          }
          break;
        case "cardAmount":
          {
            message = required.cardAmount;
          }
          break;
      }
      return message;
    }
  }
}
