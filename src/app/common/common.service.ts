import { Component, PLATFORM_ID, Injectable, NgZone, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { ToasterService, ToasterConfig, Toast } from 'angular2-toaster';
import { Observable, of } from 'rxjs';
import { NavigationEnd } from "@angular/router";
import { config } from '../../assets/config/configs';
import swal from 'sweetalert2';
import { Router } from "@angular/router";
import * as socketIo from 'socket.io-client';
import { NgxSpinnerService } from 'ngx-spinner';
import { log } from 'util';
@Injectable({
  providedIn: "root"
})
export class CommonService {
  authorised: any = false;
  public routeUrl;
  constructor(
    public _http: HttpClient,
    @Inject(PLATFORM_ID) platformId: Object,
    public router: Router,
    public toasterService: ToasterService,
    public spinner:NgxSpinnerService
  ) {
    this.platformId = platformId;
    this._apiUrl = this.config.apiUrl;
    this._socketUrl = this.config.socketUrl;
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
      }
    })
  }

  public swal = swal;
  public config = <any>config;
  public _apiUrl = "";
  public _socketUrl = "";
  public platformId;

  public getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      return window.localStorage.getItem(key);
    }
  }
  public setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.setItem(key, value);
    }
  }

  /*******************************************************************************************
      @PURPOSE      	: 	Call api.
      @Parameters 	: 	{
            url : <url of api>
            data : <data object (JSON)>
            method : String (get, post)
            isForm (Optional) : Boolean - to call api with form data
            isPublic (Optional) : Boolean - to call api without auth header
          }
  /*****************************************************************************************/
  callApi(url, data, method, pagination?, isForm?, isPublic?): Promise<any> {
    console.log("url, data, method, pagination?, isForm?, isPublic?",url, data, method, pagination, isForm, isPublic);
    let headers;
		if(isPublic) {
			headers = new HttpHeaders({ 'content-Type': 'application/json', 'language': 'EN'});
		}else if( !isPublic && !isForm){
      headers = new HttpHeaders({ 'content-Type': 'application/json', 'language': 'EN', 'Authorization': this.getToken('accessToken')});
      console.log("headers", headers);
    }

		else if(!isPublic && isForm){
  console.log("inside form");
			headers = new HttpHeaders({'language': 'EN', 'Authorization': this.getToken('accessToken')});
		}
    return new Promise((resolve, reject) => {
      if (method === "post") {
        this._http.post(this._apiUrl + url, data,  { headers }).subscribe(
          (data1: any) => {
            console.log("error",data1);
            if(data1.settings){
            if (data1.settings.message == "Cannot read property 'id' of null") {

            } else {
              resolve(data1);
            }
          } else{
            resolve(data1)
          }
          },
          error => {
            console.log("error",error);
            this.showServerError(error);
          }
        );
      } else  if (method === "put") {
        this._http.put(this._apiUrl + url, data,  { headers }).subscribe(
          (data1: any) => {
            if(data1.settings){
            if (data1.settings.message == "Cannot read property 'id' of null") {

            } else {
              resolve(data1);
            }
          }else{
            resolve(data1)
          }
          },
          error => {
            this.showServerError(error);
          }
        );
      }
       else if (method === 'get') {
        // let params: { appid: 'id1234', cnt: '5' }

        if (data) {
          let params = new HttpParams();
          Object.keys(data).forEach(function (key) {
            params = params.set(key, data[key]);
          });
          this._http
            .get(this._apiUrl + url, { params }, )
            .subscribe(
              (data1: any) => {

                if (data1.settings.message == "Cannot read property 'id' of null") {
                  this.logout();
                } else {
                  resolve(data1);
                }
              },
              error => {

                this.showServerError(error);
              }
            );
        } else {
        console.log("elsssssssee");
          this._http
            .get(this._apiUrl + url , { headers })
            .subscribe(
              (data1: any) => {

                if (data1.settings.message == "Cannot read property 'id' of null") {
                  this.logout();
                } else {
                  resolve(data1);
                }
              },
              error => {
                this.showServerError(error);
              }
            );
        }


      }
    });
  }

  /*****************************************************************************************/
  // @PURPOSE      	: 	To show server error
  /*****************************************************************************************/
  showServerError(e) {
    if (e.status == 401) {
      this.logout();
    } else {
      this.swal({
        position: 'center',
        type: 'error',
        text: 'Something went wrong',
        showConfirmButton: false,
        timer: 1800,
        customClass: 'custom-toaster'
      })
    }
    this.spinner.hide();
  }
  /****************************************************************************/
  /*****************************************************************************************/
  // @PURPOSE      	: 	To show server error
  /*****************************************************************************************/
  logout() {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.clear()
    }
    this.router.navigate(["/"]);
    this.swal({
      position: 'center',
      type: 'error',
      text: 'Your session is expired. Please login again.',
      showConfirmButton: false,
      timer: 1800,
      customClass: 'custom-toaster'
    })
  }
  /****************************************************************************/
  /*****************************************************************************************/
  // @PURPOSE      	:  Socket Connection
  /*****************************************************************************************/
  private socket;
  observer
  startSocket() {
    this.socket = socketIo(this._socketUrl)
  }
  socketEmit(sendKey, data) {
    this.socket.emit(sendKey, data);
  }
  notificationOrderData
  // public toasterService: ToasterService;
  popToaster(type, title, body, url) {

    if (url) {
      var toast: Toast = {
        type: type,
        title: title,
        body: body,
        showCloseButton: true,
        clickHandler: (t, isClosed): boolean => {
          if (!isClosed) {
            if (url) {
              this.router.navigate([url])
            }
          }
          return true; // remove this toast !
        }
      };
    }
    else {
      var toast: Toast = {
        type: type,
        title: title,
        body: body,
        showCloseButton: true,
      };
    }
    // notification sound
    const audio = new Audio('assets/sound/notification_tone.mp3');
    audio.play();

    this.toasterService.pop(toast);
  }
  socketListenOrder(): Observable<any> {
    this.socket.on('orderReceive', (res) => {
      this.notificationOrderData = res;
      this.notificationOrderData.totalbtc = parseInt(this.notificationOrderData.totalbtc);
      this.notificationOrderData.totalnaira = parseInt(this.notificationOrderData.totalnaira);
      this.setToken('btc', this.notificationOrderData.totalbtc.toString())
      this.setToken('naira', this.notificationOrderData.totalnaira.toString())
      this.setToken('notificationCount', this.notificationOrderData.unreadCount);
      if (this.notificationOrderData.title == 'Trade Notification') {
        this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.message, '/user/transaction-history/' + this.notificationOrderData.id + '');
      } else {
        this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.message, '/user/withdrawal-history');
      }
      this.notificationOrderData.totalbtc = parseInt(this.notificationOrderData.totalbtc);
      this.notificationOrderData.totalnaira = parseInt(this.notificationOrderData.totalnaira);
      if (this.routeUrl == '/user/transaction-history') {
        this.router.navigate(['/user/transaction-history']);
      }
      this.observer.next(res);
    });
    this.socket.on('withdrawReceive', (res) => {
      this.notificationOrderData = res;
      this.notificationOrderData.totalbtc = parseInt(this.notificationOrderData.totalbtc);
      this.notificationOrderData.totalnaira = parseInt(this.notificationOrderData.totalnaira);
      this.setToken('btc', this.notificationOrderData.totalbtc.toString())
      this.setToken('naira', this.notificationOrderData.totalnaira.toString())
      this.setToken('notificationCount', this.notificationOrderData.unreadCount);
      if (this.notificationOrderData.title == 'Trade Notification') {
        this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.message, '/user/transaction-history/' + this.notificationOrderData.id + '');
      } else {
        this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.message, '/user/withdrawal-history');
      }
      this.notificationOrderData.totalbtc = parseInt(this.notificationOrderData.totalbtc);
      this.notificationOrderData.totalnaira = parseInt(this.notificationOrderData.totalnaira);
      if (this.routeUrl == '/user/withdrawal-history') {
        this.router.navigate(['/user/withdrawal-history']);
      }
      this.observer.next(res);
    });
    this.socket.on('NormalReceive', (res) => {
      this.notificationOrderData = res;
      if (this.notificationOrderData.unreadCount) {
        this.setToken('notificationCount', this.notificationOrderData.unreadCount);
      }
      this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.description, '');

      this.observer.next(res);
    });
    this.socket.on('adminReceive', (res) => {
      this.notificationOrderData = res;
      this.setToken('isAdminStatus', this.notificationOrderData.isAdminStatus)
      this.setToken('isWithdrawStatus', this.notificationOrderData.isWithdrawStatus)
      this.observer.next(res);
    });


    this.socket.on('btcTradeReceive', (res) => {
      this.notificationOrderData = res;
      if (this.notificationOrderData.unreadCount) {
        this.setToken('notificationCount', this.notificationOrderData.unreadCount);
      }
      this.popToaster('info', this.notificationOrderData.title, this.notificationOrderData.message, '/user/trade-history/' + this.notificationOrderData.id + '');
      if (this.routeUrl.includes('/user/trade-history/')) {
        this.router.navigate(['/user/trade-history', this.notificationOrderData.id]);
      }
      if (this.routeUrl == '/user/trade-history') {
        this.router.navigate(['/user/trade-history']);
      }
      this.observer.next(res);
    });
    this.socket.on('dataReceive', (res) => {
      this.router.navigate([this.routeUrl])
      this.observer.next(res);
    });
    return this.getSocketDataObservableOrder();
  }
  getSocketDataObservableOrder(): Observable<any> {
    return new Observable(observer => {

      this.observer = observer;
    });
  }
  /****************************************************************************/
}


