import { Injectable, Injector} from '@angular/core';
import { BaseComponent } from './../common/commonComponent';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanDeactivate } from '@angular/router';
import { NavigationEnd, NavigationStart, GuardsCheckEnd } from "@angular/router";

/****************************************************************************
@PURPOSE      : Dont allow public pages to get accessed. (After Login)
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanLoginActivate extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        
        if(!this.getToken("accessToken")){
          console.log("no token");
          return true;
        }
        else{
            if(this.getToken('profileRole')=='USR'){
            this.router.navigate(['/user/dashboard']);
             return true
            }
        }
    }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : Dont allow authirized pages to get accessed.  (Before Login)
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivate extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken")){
          return true;
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/


/****************************************************************************
@PURPOSE      : Guard for the User's authenticated pages
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivateUser extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken")){
          if( this.getToken("profileRole") === 'USR')        
              return true;
          else
              this.router.navigate(['/user/dashboard']);
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : Guard for the Partner's authenticated pages
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivatePartner extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken")){
            if( this.getToken("profileRole") === 'PTR')        
              return true;
            else
              this.router.navigate(['/partner/ledger']);
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : Guard for the Partner's authenticated pages
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivateSP extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken")){
          if( this.getToken("profileRole") === 'SP')        
              return true;
          else
              this.router.navigate(['/service-provider/dashboard']);
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : Guard for the Partner's authenticated pages
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivateESP extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken")){
          if( this.getToken("profileRole") === 'ESP')        
              return true;
          else
              this.router.navigate(['/escrow-service-provider/dashboard']);
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : Guard for home page
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanHomeActivate extends BaseComponent implements CanActivate {
    constructor(inj : Injector) { super(inj)}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(this.getToken("accessToken") ){
            if(this.getToken("profileRole") === 'USR')
               this.router.navigate(['/user/dashboard']);

            if(this.getToken("profileRole") === 'PTR')
               this.router.navigate(['/partner/ledger']);

            if(this.getToken("profileRole") === 'SP')
               this.router.navigate(['/service-provider/dashboard']);

            if(this.getToken("profileRole") === 'ESP')
               this.router.navigate(['/escrow-service-provider/dashboard']);

          return true;
        }
        
        this.router.navigate(['/home']);
        return true
    }
}
/****************************************************************************/



