import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as socketIo from 'socket.io-client';
@Injectable()
export class SocketDataService {
    private socket;
    constructor(private http: HttpClient) {
        
    }
    observer
    getSocketData(url,sendKey,getKey,data): Observable<any> {
        this.socket = socketIo(url)
        this.socket.emit(sendKey, data);
        this.socket.on(getKey, (res) => {
            this.observer.next(res);
        });
        return this.getSocketDataObservable();
    }
    getSocketDataObservable(): Observable<any> {
        return new Observable(observer => { 
            this.observer = observer;
        });
    }
}