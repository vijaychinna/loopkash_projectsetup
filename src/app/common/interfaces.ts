export interface IBreadcrumbs{

    localeKey: string;
    url: string;
}

export  interface ITableSetupData{
    cols: Array<IColData>;
    isCheckbox: boolean;
    actions? : Array<string>; 
    isCollapsible? : boolean;
}

export interface IColData {
    type: string;
    colName: string;
    colFieldname: string;
    data?: Array<IReceiveOnData>;
}
export interface IReceiveOnData {
    name: string;
    code: string;
}
