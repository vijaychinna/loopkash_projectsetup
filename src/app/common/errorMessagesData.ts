export const messages =
    {
   "ERROR": {
        "REQUIRED": {
            "Email" : "Enter email address",
            "Address" : "Enter address",
            "Accountname":"Enter account name",
            "Accountnumber":"Enter account number",
            "Password" : "Enter password",
            "UserRole" : "Select User Role",
            "Username" : "Enter full name",
            "Name" : "Enter name",
            "Comment" : "Enter comment",
            "currentPassword" : "Enter current password",
            "newPassword" : "Enter new password",
            "confirmPassword" : "Enter confirm password",
            "BankId" : "Select bank",
            "category" : "Select category",
            "subCategory" : "Select sub category",
            "cardAmount" : "Enter card amount",
            "currency" : "Select currency",
            "paypal" : "Enter your paypal account",
            "quickpay" : "Enter your quickpay account",
            "firstname" : "Enter first Name",
            "lastname" : "Enter last Name",
            "mobile" : "Enter mobile number",
            "technology": "Select Technology",
            "city": "Enter City",
            "state": "Enter State",
            "country": "Enter Country"
        },
        "PATTERN": {
            "Age" : "Please enter proper Age",
            "Mobile" : "Phone number can not be less than 4 characters",
            "Email" : "Please enter a valid email address",
            "Password" : "Please provide valid password",
            "Username" : "Invalid Username",
            "Contactno" : "Invalid Contact Number",
            "cardAmount" : "Please enter valid amount"
        },
        "MINLENGTH": {
            "mobile" : "Mobile number should be 10 digits",
            "Password" : "Password must be atleast 6 characters",
            "Distance" : "Distance should be minimum 1 characters",
            "PinNo" : "Please enter minimum 6 pin number",
            "FirstName" : "Distance should not be more than 5 chartacters",
            "LastName" : "Distance should not be more than 5 chartacters",
            "Username" : "Username must be atleast 5 characters"
        },
        "CUSTOM": {
            "ConfirmPassword" : "Confirm password does not match!",
            "Location" : "Please enter valid location",
            "Subject" : "Please select subject from available list"
        }
    }
}

