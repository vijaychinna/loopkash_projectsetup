import { Component, OnInit, PLATFORM_ID, Injector, NgZone, APP_ID } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { CommonService } from './common.service';
import { ErrorMessages } from './errorMessages';
import { ToasterService, ToasterConfig ,Toast} from 'angular2-toaster';
import { HttpClient } from '@angular/common/http';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Location } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {URLConstants} from './constants/url-constants';

import swal from 'sweetalert2'
import * as jQuery from 'jquery';

declare var $: any;

@Component({
  selector: 'parent-comp',
  template: ``,
  providers: []
})

export class BaseComponent {

  routeUrl:any;
  activatedRoute: ActivatedRoute;
  errorMessage: ErrorMessages
  modalService: BsModalService
  _location;
  // swal = swal;
  titleService: Title
  metaService: Meta
  translate: TranslateService
  platformId: any;
  appId: any;
  http = this.http;
  router: Router;
  commonService: CommonService;
  baseUrl;
  $ = jQuery;
  user: any = {};
  spinner: NgxSpinnerService
  bsModalRef: BsModalRef
  profileRole: any;
  zone : NgZone;
  UrlConstants = URLConstants;
  currentYear: any=new Date().getFullYear();
  roles = [
    {name:'Service Provider',code : 'SP'},
    {name:'Escrow',code : 'ESP'},
    {name:'User',code : 'USR'},
    {name:'Partner',code : 'PTR'},
    {name:'Project Manager', code:'PM'}
  ];

  constructor(injector: Injector) {


    this.router = injector.get(Router)
    this.platformId = injector.get(PLATFORM_ID)
    this.appId = injector.get(APP_ID)
    this.zone = injector.get(NgZone)
    this.commonService = injector.get(CommonService)
    this._location = injector.get(Location);
    this.errorMessage = injector.get(ErrorMessages)
    this.http = injector.get(HttpClient)
    this.titleService = injector.get(Title)
    this.metaService = injector.get(Meta)
    this.activatedRoute = injector.get(ActivatedRoute)
    this.modalService = injector.get(BsModalService)
    this.bsModalRef = injector.get(BsModalRef)
    this.toasterService = injector.get(ToasterService);
    this.translate = injector.get(TranslateService);

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use('en');


    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((event: NavigationEnd) => {
        window.scroll(0, 0);
        this.routeUrl = event.urlAfterRedirects;
      });
    }

    this.baseUrl = this.commonService._apiUrl;
    this.spinner = injector.get(NgxSpinnerService)
  }

  ngOnInit(){
  }

  ngAfterViewInit(){
    this.animateInput();
  }

//SET SELECTED LANGUAGE
  setLanguage(lang){
     this.translate.use(lang);
  }

  //GET PROFILE ROLE
  getProfileRole(){
    return localStorage.getItem('profileRole');
  }


  // *************************************************************//
  //@Purpose : To check server or browser
  //*************************************************************//
  isBrowser() {
    if (isPlatformBrowser(this.platformId)) {
      return true;
    } else {
      return false;
    }
  }

  isAuthenticated(){
    return this.getToken('accessToken') ? true :false;
  }

  // *************************************************************//
  //@Purpose : We can use following function to use localstorage
  //*************************************************************//
  setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.setItem(key, value);
    }
  }
  getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      return window.localStorage.getItem(key);
    }
  }
  removeToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.removeItem(key);
    }
  }
  clearToken() {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.clear()
    }
  }
  //*************************************************************//

  //*************************************************************//
  //@Purpose : We can use following function to use Toaster Service.
  //*************************************************************//
  popToast(type, title) {

    // var toast: Toast = {
    //   type: type,
    //   title: title,
    //   showCloseButton: true,
    //   timeout: 3000
    // };
    // this.toasterService.pop(toast);

    swal({
      position: 'center',
      type: type,
      text: title,
      showConfirmButton: false,
      timer: 4000,
      customClass: 'custom-toaster'
    })
  }
  public toasterService: ToasterService;
  popToaster(type, title, body,url) {
    var toast: Toast = {
      type: type,
      title: title,
      body: body,
      showCloseButton: true,
      clickHandler: (t, isClosed): boolean => {
        if (!isClosed) {
          if(url){
          this.router.navigate([url])
          }
        }
        return true; // remove this toast !
      }
    };
    const audio = new Audio('assets/sound/notification_tone.mp3');
    audio.play();
    this.toasterService.pop(toast);
  }
  /****************************************************************************
  @PURPOSE      : To restrict or allow some values in input.
  @PARAMETERS   : $event
  @RETURN       : Boolen
  ****************************************************************************/
  RestrictSpace(e) {
    if (e.keyCode == 32) {
      return false;
    } else {
      return true;
    }
  }

  AllowNumbers(e) {
    var input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 43 || e.which === 45) {
      return true;
    }
    if (e.which === 36 || e.which === 35) {
      return true;
    }
    if (e.which === 37 || e.which === 39) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  AllowChar(e) {
    if ((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode == 8) {
      return true
    } else {
      return false
    }
  }
  /****************************************************************************/


  /****************************************************************************/
  //@Logout
  /****************************************************************************/
  logout() {
    // this.commonService.callApi('logout', { userId: this.getToken("_id") }, 'get').then(success => {
    //   this.clearToken()
    //   this.router.navigate(["/"]);
    // })
    this.clearToken();
    this.router.navigate(["/public/login"]);
  }
  /****************************************************************************/

  /****************************************************************************
  @PURPOSE      : To show validation message
  @PARAMETERS   : <field_name, errorObj?>
  @RETURN       : error message.
  ****************************************************************************/
  showError(field, errorObj?) {
    return this.errorMessage.getError(field, errorObj)
  }
  /****************************************************************************/
  getProfile() {
    const url = this.getToken("profileimage");
    if (url == null || url === '') {
      return 'assets/images/NoProfile.png'
    } else {
      return url;
    }
  }

  /****************************************************************************
  //For Side menu toggle
  /****************************************************************************/
  removeOpen() {
    //$("header nav").removeClass("open");
    $(".nav-toggle").removeClass("active");
    $('body').removeClass('body-hidden');
  }
  /****************************************************************************/

  /*************************************************************/
  // Copy Text to Clipboard
  /*************************************************************/
  copyToClip(str, message) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.popToast('success', message);
  }
  /****************************************************************************/
   // *************************************************************************** //
  // Round Function
  // *************************************************************************** //
  round(data) {
    return Math.round(data)
  }
  // *************************************************************************** //
  /*=====Input Animation Effect=======*/
  animateInput(){
    $(".input-effect .form-control").blur(function(e){
      //console.log("eeee",$(e.target))
      var inputValue = $(e.target).val();
      if ( inputValue == "" ) {
        $(e.target).removeClass('has-content');
        $(e.target).removeClass('focused');
      } else {
        $(e.target).addClass('has-content');
      }
    })

  }


  /****************************************************************************/
    /**
     * Translate string in user selected language from language Json file
     * @param data
     */
     translateString(data) {
         return new Promise((resolve, reject) => {
             this.translate.get(data).subscribe((res: string) => {
                 console.log("res", res);
                 resolve(res);

             });
         })
     }
}
