import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { IBreadcrumbs,ITableSetupData } from './../../common/interfaces';

@Component({
  selector: 'app-my-activity',
  templateUrl: './my-activity.component.html',
  styles: []
})
export class MyActivityComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  breadcrumbs: IBreadcrumbs[];
  tableSetupData: ITableSetupData = {cols:[], isCheckbox: false,actions :null,isCollapsible:null };
  selectedRecipient:any;

  ngOnInit() {

  	//Translate column names before passing them to data table

  	 this.translateString(['admin_define.AssetsList', 'admin_define.AssetType']).then((res) => {
            if (res) {
            	// console.log("translated res--",res);
            }
        });

     this.setBreadcrumbs();
     this.setDTableInitialData();
  }

  setDTableInitialData(){
    let tempData=[
        // {type : 'checkbox',colName:'',colFieldname:''},
        {type : 'text',colName:'bill#',colFieldname:'bill'},
        {type : 'text',colName:'total-earning',colFieldname:'totalEarning'},
      
        {type : 'text',colName:'status',colFieldname:'status'},
        {type : 'text',colName:'paid-date', colFieldname:'paidDate'},
        {type : 'text',colName:'due-date',colFieldname:'dueDate'},
    ]
    this.tableSetupData.cols=tempData;
    this.tableSetupData.isCheckbox=false;
    // this.tableSetupData.actions=['view'];
    console.log("this.tableSetupData",this.tableSetupData   );
  }

  public setBreadcrumbs() {
        this.breadcrumbs = [
            {localeKey: 'my-account', url: null},
            {localeKey: 'my-activities', url: null},
        ];
    }

}
