import { Component, OnInit,Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';

@Component({
  selector: 'app-add-recipient',
  templateUrl: './add-recipient.component.html',
  styles: []
})
export class AddRecipientComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  ngOnInit() {
    console.log("bhumikakakkakakkakakkk");
  }


   public title = 'Places';
  public addrKeys: string[];
  public addr: object;

  //Method to be invoked everytime we receive a new instance
  //of the address object from the onSelect event emitter.
  setAddress(addrObj) {
    //We are wrapping this in a NgZone to reflect the changes
    //to the object in the DOM.
    this.zone.run(() => {
      this.addr = addrObj;
      this.addrKeys = Object.keys(addrObj);
    });
  }


}
