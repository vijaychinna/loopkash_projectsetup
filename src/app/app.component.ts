import { Component, OnInit, Injector, ViewContainerRef } from '@angular/core';
import { TransferState, makeStateKey, Title, Meta } from '@angular/platform-browser';
import { NavigationEnd } from "@angular/router";
import { BaseComponent } from './common/commonComponent';
import { ToasterConfig } from 'angular2-toaster';
import * as jQuery from 'jquery';
import { DebugContext } from '@angular/core/src/view';
declare var jquery: any;
declare var $: any;


const DOGS_KEY = makeStateKey('dogs');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends BaseComponent implements OnInit {

  public showHeader: boolean = true;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  public toasterconfig: ToasterConfig = new ToasterConfig({
      showCloseButton: false,
      tapToDismiss: true,
      timeout: 3000,
      limit: 1,
    });

  constructor(inj: Injector) {
    super(inj)

    this.router.events.subscribe((event: NavigationEnd) => {
      window.scroll(0, 0);
      this.routeUrl = event.urlAfterRedirects;
      // if (this.routeUrl) {
      //   if (this.routeUrl.includes('resetpassword')) {
      //     this.showHeader = false;
      //   }else if(this.routeUrl.includes('confirmpage')){
      //     this.showHeader = false;
      //   }else{
      //     this.showHeader = true;
      //   }
      // }
    });
  }

  ngOnInit() {
    this.titleService.setTitle('Loopkash : Money Transfer');
    this.metaService.addTag({ name: 'description', content: 'Loopkash : Never Send Money Across Borders Again' });
  }


}
