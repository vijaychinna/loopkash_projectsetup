import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { IBreadcrumbs,ITableSetupData } from './../../common/interfaces';
@Component({
  selector: 'app-service-provider-dashboard',
  templateUrl: './sp-dashboard.component.html',
  styles: []
})
export class ServiceProviderDashboardComponent extends BaseComponent implements OnInit {

  constructor(inj: Injector) {super(inj) ;}

  breadcrumbs: IBreadcrumbs[];
  tableSetupData: ITableSetupData = {cols:[], isCheckbox: false,actions :null };
  selectedRecipient:any;

  ngOnInit() {

  	//Translate column names before passing them to data table

  	 this.translateString(['admin_define.AssetsList', 'admin_define.AssetType']).then((res) => {
            if (res) {
            	// console.log("translated res--",res);
            }
        });

     this.setBreadcrumbs();
     this.setDTableInitialData();
  }

  setDTableInitialData(){
    let tempData=[
        // {type : 'checkbox',colName:'',colFieldname:''},
        {type : 'text',colName:'usrename',colFieldname:'username'},
        {type : 'text',colName:'job-title',colFieldname:'jobTitle'},
        {type : 'text',colName:'bid-status',colFieldname:'bidStaus'},
        {type : 'text',colName:'user-final-response',colFieldname:'userResponse'},
        {type : 'text',colName:'payment-status', colFieldname:'paymentStatus'},
        {type : 'action',colName:'',colFieldname:'action'},
    ]
    this.tableSetupData.cols=tempData;
    this.tableSetupData.isCheckbox=false;
    this.tableSetupData.actions=['view'];
    console.log("this.tableSetupData",this.tableSetupData   );
  }

  public setBreadcrumbs() {
        this.breadcrumbs = [
            {localeKey: 'send-money', url: null},
            {localeKey: 'create-offer', url: null},
        ];
    }
}
