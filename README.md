# Loopkash App Website

## Prequisites (Development):

| Module | Version |
| --- | --- |
| Node | 8.9.4 |
| Npm | 6.2.0 |
| Angular-cli | 6.0.8 |
| Angular | 6.0.4 |


------------



##### Take Clone of project
> $ git clone -b branch_name git_url  folder_name

##### Development Process
> $ npm install

> rename configs-sample.ts to configs.ts

> $ cd src/assets/configs

> $ mv configs-sample.ts configs.ts

> $ ng serve


------------


### Deployment Process
>  $ git clone -b branch_name git_url  folder_name

>  $ npm install

> rename configs-sample.ts to configs.ts

> $ cd src/assets/configs

> $ mv configs-sample.ts configs.ts

> Create the build:
> $ npm run build
> "dist" folder will be created.

> Open AWS EC2 Server & run start pm2 instance:

> $ pm2 start dist/server.js --name="project_name"

------------

